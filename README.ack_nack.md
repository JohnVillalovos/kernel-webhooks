# ACK/NACK Webhook

You can run the webhook manually on a merge request URL with the command:

    python3 -m webhook.ack_nack \
        --merge-request https://gitlab.com/group/repo/-/merge_requests/1 \
        --owners-yaml owners.yml \
	--rhkernel-src /path/to/rhel/kernel/tree

The [main README](README.md#running-a-webhook-for-one-merge-request) describes
some common environment variables that can be set that are applicable for all
webhooks.

The `--linux-src` argument can also be passed to the script via the `LINUX_SRC`
environment variable.

## Verbose Logging

This webhook manages the `ack_nack` scoped label on the merge request.
Additional logging can be enabled by leaving a comment on the merge request
that starts with either `request-evaluation` or `request-ack-nack-evaluation`.
The former command will request evaluation from most webhooks.

## ACK Preservation across MR updates

The hook attempts to be intelligent about when to invalidate an existing ack.

Historically, when a new patchset was posted, all prior acks were lost, but
patchsets never included dependencies, maintainers would often refresh a set
as needed to be able to merge it, and developers relied upon maintainers to
update patch descriptions in-place.

With GitLab merge requests, developers actually MUST include dependent patches
in their trees, or they won't build, but they may need to rebase as their
underlying dependencies change and/or get merged into main. Developers can now
make their own edits to commit descriptions, fixing up missing metadata, but
that results in a new patchset revision.

In the historical case, underlying dependency changes were opaque to the
content actually submitted by the developer and the maintainer refreshing
their patchset made it easy to not require new acks for trivial changes. Out
of the box, GitLab's native "Approvals" system requires re-approval any time
the content of a merge request changes at all. We have opted to work around
that by tracking Acked-by comments instead (which pressing the Approval button
can generate), and then make informed decisions about whether or not to
invalidate them when a merge request is updated in some way.

There are two distinctly different types of merge requests when it comes to
ack preservation. First, the standard case, where a developer submits a merge
request, and it has no dependencies. The second and far more complicated case
is when a merge request has dependencies in it.

In both cases, we intend to preserve an ack if a merge request is updated, but
there are no code changes made. Updates to the merge request description,
commit descriptions, or a rebase onto main where none of the merge request's
code has changed, should not result in any acks being lost.

In the case of a merge request with dependencies, things can get far more
complicated, because of limitations to dependency tracking in git and gitlab.
Basically, all we have to go on for a dependencies delimiter is a git commit
sha, which can change as the merge request is updated. From here, how the
logic for "did my code change?" gets fairly technical, but the end result is
that we attempt to treat these merge requests the same with respect to ack
preservation.

## ACK preservation technical details when dependencies are involved

The primary code responsible for determining if a merge request's code has
changed or not is [webhook/cdlib.py::mr_code_changed()](webhook/cdlib.py#L332).

First up, this code relies upon a local git cache of Red Hat kernel data,
including all pending merge requests. Unfortunately, some of the merge request
data is a bit hard to come by. When someone updates their MR via a force-push,
old refs are lost, and no longer fetchable via git, so we update the local git
cache every time a merge request is updated and includes a Dependencies::sha
label, giving us permanent access to the relevant revision's refs permanently.
These are needed to generate full MR revision diffs that omit dependencies, so
we can compare version X with version X-1 exclusive of the code changes that
are marked as dependencies.

Next up, we fetch all the diff IDs from the merge request. Each revision to a
merge request creates a new diff ID. If there are fewer than two diff IDs, we
know there's been no code changes, and acks should be retained.

If we have multiple diff IDs, then we want to look at the two most recent IDs,
and figure out the ranges of commits within those two IDs that we need to care
about (i.e., filter out dependencies). For the latest ID, we simply look at
the current Dependencies scoped label. For the prior ID, we have to walk the
list of label events, searching for the most recent label remove event for a
label starting with `Dependencies::` to determine it's range. In either case,
if the label has a scope of OK, we can fetch the full revision diff from the
gitlab api. Otherwise, we need to generate a diff from the local git cache
we're maintaining, starting from the sha provided as the scope of the label,
to the HEAD commit of that revision.

Once we have the two diffs, we simple compare the two with the same function
we use to compare submitted patches against referenced upstream commits, and
if there's no relevant differences, we consider the two revisions equivalent,
and preserve any acks. Any code changes at all here will result in acks being
dropped, and the merge request needing to be re-reviewed.
