# Signoff Webhook

You can run the webhook manually on a merge request URL with the command:

    python3 -m webhook.signoff \
        --merge-request https://gitlab.com/group/repo/-/merge_requests/1 \

The [main README](README.md#running-a-webhook-for-one-merge-request) describes
some common environment variables that can be set that are applicable for all
webhooks.

## Verbose Logging

This webhook manages the `Signoff` scoped label on the merge request. By
default, a summary table is not logged if the status is OK. Additional
logging can be enabled by leaving a comment on the merge request that starts
with either `request-evaluation` or `request-signoff-evaluation`. The former
command will request evaluation from most webhooks.
