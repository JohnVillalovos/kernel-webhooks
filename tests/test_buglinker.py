"""Webhook interaction tests."""
import copy
import os
import unittest
from unittest import mock

from bugzilla import BugzillaError

import webhook.buglinker
import webhook.common


@mock.patch('cki_lib.gitlab.get_token', mock.Mock(return_value='TOKEN'))
class TestBuglinker(unittest.TestCase):
    """ Test Webhook class."""

    PAYLOAD_MERGE = {'object_kind': 'merge_request',
                     'project': {'id': 1,
                                 'web_url': 'https://web.url/g/p'},
                     'object_attributes': {'target_branch': 'main', 'iid': 2},
                     'state': 'opened',
                     'labels': []
                     }

    PAYLOAD_PIPELINE = {'object_kind': 'pipeline',
                        'project': {'id': 1,
                                    'path_with_namespace': 'group/project'},
                        'object_attributes': {'id': 22334455,
                                              'status': 'success'},
                        'merge_request': {'iid': 10,
                                          'title': 'a test merge request'}
                        }

    BZ_RESULTS = [{'id': 1234567,
                   'external_bugs': [{'ext_bz_bug_id': '111222',
                                      'type': {'description': 'Linux Kernel',
                                               'url': 'https://bugzilla.kernel.org'
                                               }
                                      },
                                     {'ext_bz_bug_id': 'redhat/rhel/8.y/kernel/-/merge_requests/10',
                                      'type': {'description': 'Gitlab',
                                               'url': 'https://gitlab.com/'
                                               }
                                      }]
                   },
                  {'id': 8675309,
                   'external_bugs': [{'ext_bz_bug_id': '11223344',
                                      'type': {'description': 'Linux Kernel',
                                               'url': 'https://bugzilla.kernel.org'
                                               }
                                      },
                                     {'ext_bz_bug_id': 'group/project/-/merge_requests/321',
                                      'type': {'description': 'Gitlab',
                                               'url': 'https://gitlab.com/'
                                               }
                                      }]
                   },
                  {'id': 66442200,
                   'external_bugs': []
                   }]

    @mock.patch('webhook.buglinker.add_mr_to_bz', return_value=True)
    def test_update_bugzilla(self, mocked_addmrtobz):
        """Check for expected calls."""
        project = mock.Mock()
        bzcon = mock.Mock()
        mr_id = 10

        # If the input bug list is empty, do nothing.
        with self.assertLogs('cki.webhook.buglinker', level='DEBUG') as logs:
            webhook.buglinker.update_bugzilla(project, [], mr_id, bzcon)
            self.assertIn('Input bug list is empty.', logs.output[-1])
            mocked_addmrtobz.assert_not_called()

        # Ignore non-numeric bugs from the input bug list.
        with self.assertLogs('cki.webhook.buglinker', level='INFO') as logs:
            webhook.buglinker.update_bugzilla(project, ['INTERNAL'], mr_id, bzcon)
            self.assertIn('bugzilla actions: [\'INTERNAL\']', logs.output[-1])
            mocked_addmrtobz.assert_not_called()

        # Call add_mr_to_bz() with the expected parameters.
        bug_list = ['252626', '739272', '1544362', 'INTERNAL']
        with self.assertLogs('cki.webhook.buglinker', level='INFO') as logs:
            webhook.buglinker.update_bugzilla(project, bug_list, mr_id, bzcon)
            self.assertIn('bugzilla actions: [\'INTERNAL\']', logs.output[-1])
            mocked_addmrtobz.assert_called_with(bzcon, project, ['252626', '739272', '1544362'], 10)

    def test_bz_is_linked_to_mr(self):
        """Check for expected return values."""
        domain = 'gitlab.com'
        path = 'redhat/rhel/8.y/kernel/-/merge_requests/10'
        bz0 = mock.Mock(id=self.BZ_RESULTS[0]['id'],
                        external_bugs=self.BZ_RESULTS[0]['external_bugs'])
        bz1 = mock.Mock(id=self.BZ_RESULTS[1]['id'],
                        external_bugs=self.BZ_RESULTS[1]['external_bugs'])
        bz2 = mock.Mock(id=self.BZ_RESULTS[2]['id'],
                        external_bugs=self.BZ_RESULTS[2]['external_bugs'])
        self.assertTrue(webhook.buglinker.bz_is_linked_to_mr(bz0, domain, path))
        self.assertFalse(webhook.buglinker.bz_is_linked_to_mr(bz1, domain, path))
        self.assertFalse(webhook.buglinker.bz_is_linked_to_mr(bz2, domain, path))

    @mock.patch.dict(os.environ, {'BUGZILLA_API_KEY': 'mocked'})
    def test_add_mr_to_bz(self):
        """Check the call to bzcon.add_external_tracker() has the expected input."""
        project = mock.Mock()
        project.web_url = 'https://gitlab.com/redhat/rhel/8.y/kernel'
        project.path_with_namespace = 'redhat/rhel/8.y/kernel'
        bz0 = mock.Mock(id=self.BZ_RESULTS[0]['id'],
                        external_bugs=self.BZ_RESULTS[0]['external_bugs'])
        bz1 = mock.Mock(id=self.BZ_RESULTS[1]['id'],
                        external_bugs=self.BZ_RESULTS[1]['external_bugs'])
        bz2 = mock.Mock(id=self.BZ_RESULTS[2]['id'],
                        external_bugs=self.BZ_RESULTS[2]['external_bugs'])
        bzcon = mock.Mock()
        merge_request = 10

        # Early return due to empty bug_list
        with self.assertLogs('cki.webhook.buglinker', level='ERROR') as logs:
            webhook.buglinker.add_mr_to_bz(bzcon, project, [], merge_request)
            self.assertIn('MR 10 has no bugs? Skipping linking of bugs to MR.', logs.output[-1])
            bzcon.add_external_tracker.assert_not_called()

        # Early return due to getbugs() returning nothing.
        bzcon.getbugs.return_value = []
        with self.assertLogs('cki.webhook.buglinker', level='INFO') as logs:
            webhook.buglinker.add_mr_to_bz(bzcon, project, ['12345678'], merge_request)
            self.assertIn("getbugs() returned an empty list for these bugs: ['12345678'].",
                          logs.output[-1])
            bzcon.add_external_tracker.assert_not_called()

        # Early return due to no untracked bugs
        bzcon.getbugs.return_value = [bz0]
        with self.assertLogs('cki.webhook.buglinker', level='INFO') as logs:
            webhook.buglinker.add_mr_to_bz(bzcon, project, [bz0.id], merge_request)
            self.assertIn('All bugs have an existing link to MR 10.', logs.output[-1])
            bzcon.add_external_tracker.assert_not_called()

        # bzcon raises an exception.
        bzcon.getbugs.return_value = [bz0, bz1, bz2]
        bzcon.add_external_tracker.side_effect = BugzillaError('oh no!')
        with mock.patch('cki_lib.misc.is_production', return_value=True):
            with self.assertLogs('cki.webhook.buglinker', level='ERROR') as logs:
                exception_hit = False
                try:
                    webhook.buglinker.add_mr_to_bz(bzcon, project, [bz0.id, bz1.id, bz2.id],
                                                   merge_request)
                except BugzillaError:
                    exception_hit = True
                self.assertFalse(exception_hit)
                ext_type_url = 'https://gitlab.com/'
                ext_bz_bug_id = 'redhat/rhel/8.y/kernel/-/merge_requests/10'
                self.assertIn(f'Problem adding tracker {ext_type_url}{ext_bz_bug_id} to BZs.',
                              logs.output[-1])
                bzcon.add_external_tracker.assert_called_with([8675309, 66442200],
                                                              ext_type_url=ext_type_url,
                                                              ext_bz_bug_id=ext_bz_bug_id)

        # Successful call to add_external_tracker()
        bzcon.add_external_tracker.side_effect = None
        with mock.patch('cki_lib.misc.is_production', return_value=True):
            webhook.buglinker.add_mr_to_bz(bzcon, project, [bz0.id, bz1.id, bz2.id], merge_request)
        ext_type_url = 'https://gitlab.com/'
        ext_bz_bug_id = 'redhat/rhel/8.y/kernel/-/merge_requests/10'
        bzcon.add_external_tracker.assert_called_with([8675309, 66442200],
                                                      ext_type_url=ext_type_url,
                                                      ext_bz_bug_id=ext_bz_bug_id)

    @mock.patch('webhook.buglinker.comment_already_posted')
    @mock.patch('cki_lib.misc.is_production', mock.Mock(return_value=True))
    def test_post_to_bugs(self, mock_comment_check):
        bzcon = mock.Mock()
        bz1 = mock.Mock(id=11223344)
        bz2 = mock.Mock(id=22334455)

        # Comment already posted to all bugs.
        with self.assertLogs('cki.webhook.buglinker', level='INFO') as logs:
            bug_list = [bz1.id, bz2.id]
            bzcon.getbugs.return_value = [bz1, bz2]
            mock_comment_check.return_value = True
            webhook.buglinker.post_to_bugs(bug_list, 'text', 'pipeline_url', bzcon)
            self.assertIn('Pipeline results have already been posted to all relevant bugs.',
                          logs.output[-1])
            self.assertEqual(mock_comment_check.call_count, 2)
            bzcon.update_bugs.assert_not_called()

        # Succesful comment, one filtered bug.
        with self.assertLogs('cki.webhook.buglinker', level='INFO') as logs:
            mock_comment_check.return_value = False
            mock_comment_check.side_effect = [False, True]
            webhook.buglinker.post_to_bugs(bug_list, 'text', 'pipeline_url', bzcon)
            bzcon.update_bugs.assert_called_with([bz1.id], bzcon.build_update())
            self.assertIn('Posted comment to bugs: [11223344]', logs.output[-1])

        # Exception.
        with self.assertLogs('cki.webhook.buglinker', level='ERROR') as logs:
            mock_comment_check.side_effect = None
            bzcon.update_bugs.side_effect = BugzillaError(message='oops')
            webhook.buglinker.post_to_bugs(bug_list, 'text', 'pipeline_url', bzcon)
            bzcon.update_bugs.assert_called_with([bz1.id, bz2.id], bzcon.build_update())
            self.assertIn('Error posting comments to bugs: [11223344, 22334455]', logs.output[-1])

    def test_comment_already_posted(self):
        pipeline_url = 'https://gitlab.com/project/-/pipelines/12345'
        pipeline_text = f'Pipeline: {pipeline_url}'
        comment1 = {'creator': 'user@redhat.com', 'text': 'cool comment', 'count': 1}
        comment2 = {'creator': webhook.buglinker.KERNEL_BZ_BOT,
                    'text': f'hey there\n{pipeline_text}\nthanks', 'count': 2}
        bug = mock.Mock
        bug.id = 678910

        # comment2 should return True.
        with self.assertLogs('cki.webhook.buglinker', level='INFO') as logs:
            bug.getcomments = mock.Mock(return_value=[comment1, comment2])
            self.assertTrue(webhook.buglinker.comment_already_posted(bug, pipeline_url))
            self.assertIn('Excluding bug 678910 as pipeline was already posted in comment 2.',
                          logs.output[-1])

        # comment1 is False.
        bug.getcomments = mock.Mock(return_value=[comment1])
        self.assertFalse(webhook.buglinker.comment_already_posted(bug, pipeline_url))

    def test_parse_gl_project_path(self):
        url1 = 'http://www.gitlab.com/mycoolproject/-/merge_requests/12345'
        url2 = 'http://www.gitlab.com/group/subgroup/project/-/pipelines/35435747'
        url3 = 'https://gitlab.com/this/is/a/test/-/issues/'

        self.assertEqual('mycoolproject', webhook.buglinker.parse_gl_project_path(url1))
        self.assertEqual('group/subgroup/project', webhook.buglinker.parse_gl_project_path(url2))
        self.assertEqual('this/is/a/test', webhook.buglinker.parse_gl_project_path(url3))

    def test_mr_event_should_run_pipeline(self):
        merge_dict = copy.deepcopy(self.PAYLOAD_MERGE)

        # MR message changes do not include 'title'.
        merge_dict['object_attributes']['work_in_progress'] = False
        merge_dict['changes'] = {}
        msg = mock.Mock(payload=merge_dict)
        self.assertFalse(webhook.buglinker.mr_event_should_run_pipeline(msg))

        # Titles don't mention WIP..
        merge_dict['changes'] = {'title': {'previous': 'My awesome MR',
                                           'current': 'My cool MR'}
                                 }
        msg = mock.Mock(payload=merge_dict)
        self.assertFalse(webhook.buglinker.mr_event_should_run_pipeline(msg))

        # No 'previous' title member
        merge_dict['changes'] = {'title': {'current': 'My cool MR'}}
        msg = mock.Mock(payload=merge_dict)
        self.assertFalse(webhook.buglinker.mr_event_should_run_pipeline(msg))

        # It's not a WIP!.
        merge_dict['changes'] = {'title': {'previous': '[WIP] My cool MR',
                                           'current': 'My cool MR'}
                                 }
        msg = mock.Mock(payload=merge_dict)
        self.assertTrue(webhook.buglinker.mr_event_should_run_pipeline(msg))

    @mock.patch('webhook.common.Message.gl_instance')
    @mock.patch('webhook.common.try_bugzilla_conn')
    @mock.patch('webhook.buglinker.post_to_bugs')
    def test_process_pipeline(self, mock_post, mock_bz, mock_gl):
        # Process a merge_request event without head_pipeline_id:
        msg = mock.Mock(payload=copy.deepcopy(self.PAYLOAD_MERGE))
        webhook.buglinker.process_pipeline(mock_gl, msg)
        mock_gl.projects.get.asset_not_called()

        # MR event, MR is a WIP.
        with self.assertLogs('cki.webhook.buglinker', level='INFO') as logs:
            merge_msg = copy.deepcopy(self.PAYLOAD_MERGE)
            msg = mock.Mock(payload=merge_msg)
            mock_project = mock.Mock(path_with_namespace='project')
            mock_gl.projects.get.return_value = mock_project
            mock_mr = mock.Mock(iid=2, description='', work_in_progress=True)
            mock_project.mergerequests.get.return_value = mock_mr

            webhook.buglinker.process_pipeline(mock_gl, msg)
            self.assertIn('MR 2 is marked work in progress, ignoring.', logs.output[-1])
            mock_post.assert_not_called()

        # Pipeline event, MR head_pipeline member is None.
        with self.assertLogs('cki.webhook.buglinker', level='INFO') as logs:
            msg = mock.Mock(payload=copy.deepcopy(self.PAYLOAD_PIPELINE))
            mock_project = mock.Mock(path_with_namespace='project')
            mock_gl.projects.get.return_value = mock_project
            mock_mr = mock.Mock(iid=10, description='', work_in_progress=False,
                                labels=['readyForQA'], head_pipeline=None)
            mock_project.mergerequests.get.return_value = mock_mr

            webhook.buglinker.process_pipeline(mock_gl, msg)
            self.assertIn("MR 10 has not triggered any pipelines? head_pipeline is None.",
                          logs.output[-1])
            mock_post.assert_not_called()

        # MR event, pipeline is in the wrong namespace.
        with self.assertLogs('cki.webhook.buglinker', level='INFO') as logs:
            merge_msg = copy.deepcopy(self.PAYLOAD_MERGE)
            msg = mock.Mock(payload=merge_msg)
            mock_project = mock.Mock(path_with_namespace='project')
            mock_gl.projects.get.return_value = mock_project
            head_pipeline = {'id': 22334455, 'status': 'running',
                             'web_url': 'https://gitlab.com/ptalbert/rhel-8/-/pipelines/22334455'}
            mock_mr = mock.Mock(iid=2, description='', work_in_progress=False,
                                labels=['readyForQA'], head_pipeline=head_pipeline)
            mock_project.mergerequests.get.return_value = mock_mr

            webhook.buglinker.process_pipeline(mock_gl, msg)
            self.assertIn('MR 2 head pipeline #22334455 is not in the Red Hat namespace',
                          logs.output[-1])
            mock_post.assert_not_called()

        # MR does not have any bugs listed.
        with self.assertLogs('cki.webhook.buglinker', level='INFO') as logs:
            merge_msg = copy.deepcopy(self.PAYLOAD_MERGE)
            msg = mock.Mock(payload=merge_msg)
            mock_project = mock.Mock(path_with_namespace='project')
            mock_gl.projects.get.return_value = mock_project
            head_pipeline = {'id': 22334455, 'status': 'success',
                             'web_url': 'https://gitlab.com/redhat/rhel-8/-/pipelines/22334455'}
            mock_mr = mock.Mock(iid=2, description='', work_in_progress=False,
                                labels=['readyForQA'], head_pipeline=head_pipeline)
            mock_project.mergerequests.get.return_value = mock_mr

            webhook.buglinker.process_pipeline(mock_gl, msg)
            self.assertIn('No bugs found in MR 2 description.', logs.output[-1])
            mock_post.assert_not_called()

        # No downstream pipeline info.
        with self.assertLogs('cki.webhook.buglinker', level='DEBUG') as logs:
            description = ('Bugzilla: https://bugzilla.redhat.com/show_bug.cgi?id=123456\n'
                           'Depends: https://bugzilla.redhat.com/show_bug.cgi?id=56789')
            head_pipeline = {'id': 22334455, 'status': 'success',
                             'web_url': 'https://gitlab.com/redhat/rhel-8/-/pipelines/22334455'}
            mock_mr = mock.Mock(iid=10, description=description, title="A Cool MR",
                                web_url="https://gitlab.com/project/-/merge_requests/10",
                                work_in_progress=False, labels=['readyForQA'],
                                head_pipeline=head_pipeline)
            mock_project.mergerequests.get.return_value = mock_mr
            bridge = mock.Mock()
            bridge.name = 'unexpected_pipeline'
            web_url = 'https://gitlab.com/group/project/-/pipelines/4321'
            bridge.downstream_pipeline = None
            pipeline = mock.Mock(id=22334455, status='success')
            pipeline.bridges.list.return_value = [bridge]
            mock_project.pipelines.get.return_value = pipeline
            mock_gl.projects.get.return_value = mock_project

            webhook.buglinker.process_pipeline(mock_gl, msg)
            self.assertIn('No downstream pipeline found for local pipeline 22334455.',
                          logs.output[-1])
            mock_post.assert_not_called()

        # Downstream pipeline status is not 'success' or 'failed'.
        with self.assertLogs('cki.webhook.buglinker', level='INFO') as logs:
            description = ('Bugzilla: https://bugzilla.redhat.com/show_bug.cgi?id=123456\n'
                           'Depends: https://bugzilla.redhat.com/show_bug.cgi?id=56789')
            head_pipeline = {'id': 22334455,
                             'web_url': 'https://gitlab.com/redhat/rhel-8/-/pipelines/22334455'}
            mock_mr = mock.Mock(iid=10, description=description, title="A Cool MR",
                                web_url="https://gitlab.com/project/-/merge_requests/10",
                                work_in_progress=False, labels=['readyForQA'],
                                head_pipeline=head_pipeline)
            mock_project.mergerequests.get.return_value = mock_mr
            bridge = mock.Mock()
            bridge.name = webhook.buglinker.KERNEL_PIPELINE
            web_url = 'https://gitlab.com/group/project/-/pipelines/4321'
            bridge.downstream_pipeline = {'id': 4321, 'web_url': web_url}
            pipeline = mock.Mock(id=22334455, status='success')
            pipeline.bridges.list.return_value = [bridge]
            mock_project.pipelines.get.return_value = pipeline
            mock_ds_project = mock.Mock()
            mock_ds_project.name = 'Cool Downstream Project'
            mock_gl.projects.get.return_value = True
            mock_gl.projects.get.side_effect = [mock_project, mock_ds_project]
            mock_ds_pipeline = mock.Mock(id=4321, status='running')
            mock_ds_project.pipelines.get.return_value = mock_ds_pipeline
            job1 = mock.Mock(stage='build', id=11)
            job2 = mock.Mock(stage='merge', id=12)
            mock_ds_pipeline.jobs.list.return_value = [job1, job2]
            msg = mock.Mock(payload=copy.deepcopy(self.PAYLOAD_PIPELINE))

            webhook.buglinker.process_pipeline(mock_gl, msg)
            self.assertIn('Pipeline 4321 is not finished: running', logs.output[-1])
            mock_project.pipelines.get.assert_called_with(22334455)
            pipeline.bridges.list.assert_called()
            mock_ds_project.pipelines.get.assert_called_with(4321)
            mock_post.assert_not_called()

        # No publish jobs.
        with self.assertLogs('cki.webhook.buglinker', level='INFO') as logs:
            description = ('Bugzilla: https://bugzilla.redhat.com/show_bug.cgi?id=123456\n'
                           'Depends: https://bugzilla.redhat.com/show_bug.cgi?id=56789')
            head_pipeline = {'id': 22334455, 'status': 'success',
                             'web_url': 'https://gitlab.com/redhat/rhel-8/-/pipelines/22334455'}
            mock_mr = mock.Mock(iid=10, description=description, title="A Cool MR",
                                web_url="https://gitlab.com/project/-/merge_requests/10",
                                work_in_progress=False, labels=['readyForQA'],
                                head_pipeline=head_pipeline)
            mock_project.mergerequests.get.return_value = mock_mr
            bridge = mock.Mock()
            bridge.name = webhook.buglinker.KERNEL_PIPELINE
            web_url = 'https://gitlab.com/group/project/-/pipelines/4321'
            bridge.downstream_pipeline = {'id': 4321, 'web_url': web_url}
            pipeline = mock.Mock(id=22334455, status='success')
            pipeline.bridges.list.return_value = [bridge]
            mock_project.pipelines.get.return_value = pipeline
            mock_ds_project = mock.Mock()
            mock_ds_project.name = 'Cool Downstream Project'
            mock_gl.projects.get.return_value = True
            mock_gl.projects.get.side_effect = [mock_project, mock_ds_project]
            mock_ds_pipeline = mock.Mock(id=4321, status='success')
            mock_ds_project.pipelines.get.return_value = mock_ds_pipeline
            mock_ds_pipeline.jobs.list.return_value = [job1, job2]
            msg = mock.Mock(payload=copy.deepcopy(self.PAYLOAD_PIPELINE))

            webhook.buglinker.process_pipeline(mock_gl, msg)
            self.assertIn('pipeline 4321 in project Cool Downstream Project', logs.output[-1])
            mock_project.pipelines.get.assert_called_with(22334455)
            pipeline.bridges.list.assert_called()
            mock_ds_project.pipelines.get.assert_called_with(4321)
            mock_post.assert_not_called()

        # No successful publish jobs
        with self.assertLogs('cki.webhook.buglinker', level='DEBUG') as logs:
            job3 = mock.Mock(stage='publish', id=13)
            job4 = mock.Mock(stage='publish', id=14)
            mock_ds_pipeline.jobs.list.return_value = [job1, job2, job3, job4]
            mock_gl.projects.get.side_effect = [mock_project, mock_ds_project]

            webhook.buglinker.process_pipeline(mock_gl, msg)
            self.assertIn(('No successful publish jobs found for downstream pipeline'
                           ' 4321 in project Cool Downstream Project.'), logs.output[-1])
            mock_post.assert_not_called()

        # No job data.
        with self.assertLogs('cki.webhook.buglinker', level='DEBUG') as logs:
            job3 = mock.Mock(stage='publish', id=13, status='success')
            job3.name = 'publish x86_64'
            job4 = mock.Mock(stage='publish', id=14, status='success')
            job4.name = 'publish s390x'
            job5 = mock.Mock(stage='test', id=15, status='running')
            job5.name = 'test x86_64'
            job6 = mock.Mock(stage='test', id=16, status='success')
            job6.name = 'test s390x'
            mock_ds_pipeline.jobs.list.return_value = [job1, job2, job3, job4, job5, job6]
            pjob3 = mock.Mock()
            pjob4 = mock.Mock()
            pjob3.artifact.return_value = None
            pjob4.artifact.return_value = None
            mock_ds_project.jobs.get.side_effect = [pjob3, pjob4]
            mock_gl.projects.get.side_effect = [mock_project, mock_ds_project]

            webhook.buglinker.process_pipeline(mock_gl, msg)
            self.assertIn('No job data.', logs.output[-1])
            mock_post.assert_not_called()

        pjob3_rc = ("[state]\n"
                    "repo_path = artifacts/repo/4.18.0-123.el8/\n"
                    "kernel_package_url = http://internal/repo/x86_64/4.18.0-123.el8.x86_64\n"
                    "kernel_arch = x86_64\n"
                    "kernel_version = 4.18.0-123.el8\n").encode()

        pjob4_rc = ("[state]\n"
                    "repo_path = artifacts/repo/4.18.0-123.el8/\n"
                    "kernel_package_url = http://internal/repo/x86_64/4.18.0-123.el8.s390x\n"
                    "kernel_arch = s390x\n"
                    "kernel_version = 4.18.0-123.el8\n").encode()

        pjob3.artifact.return_value = pjob3_rc
        pjob4.artifact.return_value = pjob4_rc
        mock_ds_project.jobs.get.side_effect = [pjob3, pjob4]
        mock_gl.projects.get.side_effect = [mock_project, mock_ds_project]

        # No bzcon.
        with self.assertLogs('cki.webhook.buglinker', level='ERROR') as logs:
            mock_bz.return_value = False
            mock_ds_project.jobs.get.side_effect = [pjob3, pjob4]
            mock_gl.projects.get.side_effect = [mock_project, mock_ds_project]
            webhook.buglinker.process_pipeline(mock_gl, msg)
            self.assertIn('No bugzilla connection.', logs.output[-1])
            mock_post.assert_not_called()

        # All good?
        with self.assertLogs('cki.webhook.buglinker', level='INFO') as logs:
            mock_bz.return_value = True
            mock_ds_project.jobs.get.side_effect = [pjob3, pjob4]
            mock_gl.projects.get.side_effect = [mock_project, mock_ds_project]
            webhook.buglinker.process_pipeline(mock_gl, msg)
            self.assertIn("Creating bug comment on bugs: ['123456']", logs.output[-1])
            mock_post.assert_called_once()

    @mock.patch('webhook.common.Message.gl_instance')
    @mock.patch('webhook.common.try_bugzilla_conn')
    @mock.patch('webhook.buglinker.mr_event_should_run_pipeline')
    @mock.patch('webhook.buglinker.process_pipeline')
    @mock.patch('webhook.common.mr_action_affects_commits')
    @mock.patch('webhook.buglinker.update_bugzilla')
    def test_process_mr(self, mock_updatebz, mock_affectscommits, mock_processp, mock_wipcheck,
                        mock_bz, mock_gl):
        # MR event: removed WIP.
        mock_wipcheck.return_value = True

        webhook.buglinker.process_mr(mock_gl, 'msg')
        mock_processp.assert_called_with(mock_gl, 'msg')

        # MR event: files have not changed.
        mock_processp.reset_mock()
        mock_wipcheck.return_value = []
        mock_affectscommits.return_value = False

        webhook.buglinker.process_mr(mock_gl, 'msg')
        mock_processp.assert_not_called()
        mock_affectscommits.assert_called_with('msg')
        mock_bz.assert_not_called()

        # No BZ connection.
        mock_affectscommits.return_value = True
        original_mock_bz = mock_bz.return_value
        mock_bz.return_value = False

        webhook.buglinker.process_mr(mock_gl, 'msg')
        mock_processp.assert_not_called()
        mock_gl.projects.get.assert_not_called()

        # No bugs in MR description.
        mock_bz.return_value = original_mock_bz
        mock_project = mock.Mock()
        mock_gl.projects.get.return_value = mock_project
        mock_mr = mock.Mock(description='', iid=2)
        mock_project.mergerequests.get.return_value = mock_mr
        msg = mock.Mock(payload=copy.deepcopy(self.PAYLOAD_MERGE))

        with self.assertLogs('cki.webhook.buglinker', level='INFO') as logs:
            webhook.buglinker.process_mr(mock_gl, msg)
            mock_processp.assert_not_called()
            mock_gl.projects.get.assert_called_with(1)
            mock_project.mergerequests.get.assert_called_with(2)
            mock_updatebz.assert_not_called()
            self.assertIn('No bugs found in MR 2 description.', logs.output[-1])

        # All good?
        description = 'Bugzilla: https://bugzilla.redhat.com/show_bug.cgi?id=12345678'
        mock_mr = mock.Mock(description=description, iid=2)
        mock_project.mergerequests.get.return_value = mock_mr

        webhook.buglinker.process_mr(mock_gl, msg)
        mock_processp.assert_not_called()
        mock_gl.projects.get.assert_called_with(1)
        mock_project.mergerequests.get.assert_called_with(2)
        mock_updatebz.assert_called_with(mock_project, ['12345678'], 2, original_mock_bz)
