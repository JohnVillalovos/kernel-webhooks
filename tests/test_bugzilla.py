"""Webhook interaction tests."""
import copy
import json
import os
import unittest
from unittest import mock

from gitlab.exceptions import GitlabGetError

import webhook.bugzilla
import webhook.common


@mock.patch('cki_lib.gitlab.get_token', mock.Mock(return_value='TOKEN'))
class TestBugzilla(unittest.TestCase):
    """ Test Webhook class."""

    PAYLOAD_MERGE = {'object_kind': 'merge_request',
                     'project': {'id': 1,
                                 'web_url': 'https://web.url/g/p'},
                     'object_attributes': {'target_branch': 'main', 'iid': 2},
                     'state': 'opened'
                     }

    PAYLOAD_NOTE = {'object_kind': 'note',
                    'project': {'id': 1,
                                'web_url': 'https://web.url/g/p'},
                    'object_attributes': {'note': 'comment',
                                          'noteable_type': 'MergeRequest'},
                    'merge_request': {'target_branch': 'main', 'iid': 2},
                    'state': 'opened'
                    }

    BZ_RESULTS = [{'id': 1234567,
                   'external_bugs': [{'ext_bz_bug_id': '111222',
                                      'type': {'description': 'Linux Kernel',
                                               'url': 'https://bugzilla.kernel.org'
                                               }
                                      },
                                     {'ext_bz_bug_id': 'redhat/rhel/8.y/kernel/-/merge_requests/10',
                                      'type': {'description': 'Gitlab',
                                               'url': 'https://gitlab.com/'
                                               }
                                      }]
                   },
                  {'id': 8675309,
                   'external_bugs': [{'ext_bz_bug_id': '11223344',
                                      'type': {'description': 'Linux Kernel',
                                               'url': 'https://bugzilla.kernel.org'
                                               }
                                      },
                                     {'ext_bz_bug_id': 'group/project/-/merge_requests/321',
                                      'type': {'description': 'Gitlab',
                                               'url': 'https://gitlab.com/'
                                               }
                                      }]
                   },
                  {'id': 66442200,
                   'external_bugs': []
                   }]

    def test_get_project_file(self):
        project = mock.Mock()
        project.files.raw = mock.Mock(return_value='raw file')
        project.files.get = mock.Mock(return_value='file')

        # Success with raw.
        result = webhook.bugzilla.get_project_file(project, 'main', 'Makefile')
        self.assertEqual(result, 'raw file')
        project.files.get.assert_not_called()

        # Success without raw.
        project.files.raw.reset_mock()
        project.files.get.reset_mock()
        result = webhook.bugzilla.get_project_file(project, 'main', 'Makefile', raw=False)
        self.assertEqual(result, 'file')
        project.files.raw.assert_not_called()

        # Exception.
        with self.assertLogs('cki.webhook.bugzilla', level='INFO') as logs:
            project.files.raw.side_effect = GitlabGetError
            result = webhook.bugzilla.get_project_file(project, 'main', 'Makefile')
            self.assertEqual(result, None)
            self.assertIn('Failed to fetch.', ' '.join(logs.output))

    @mock.patch('webhook.bugzilla.get_project_file')
    def test_get_project_major_ver(self, mock_get_file):
        project = mock.Mock()

        # Project name is 'rhel-#' format or rhel-alt-#.
        project.name = 'rhel-7'
        result = webhook.bugzilla.get_project_major_ver(project)
        self.assertEqual(result, '7')
        project.name = 'rhel-10-alpha'
        result = webhook.bugzilla.get_project_major_ver(project)
        self.assertEqual(result, '10')
        project.name = 'rhel-alt-7'
        result = webhook.bugzilla.get_project_major_ver(project)
        self.assertEqual(result, '7')

        # Try and fail to get the makefile.
        with self.assertLogs('cki.webhook.bugzilla', level='DEBUG') as logs:
            project.name = 'kernel-test'
            mock_get_file.return_value = None
            result = webhook.bugzilla.get_project_major_ver(project)
            self.assertEqual(result, None)
            self.assertIn(("Unable to determine major version of project"
                           " 'kernel-test' from its name."), logs.output[-1])
            self.assertNotIn('Cannot find RHEL_MAJOR in Makefile.', ' '.join(logs.output))
            self.assertEqual(mock_get_file.call_count, 2)

        # Get version from file.
        with self.assertLogs('cki.webhook.bugzilla', level='DEBUG') as logs:
            project.name = 'kernel-test'
            mock_get_file.side_effect = [None, b'RHEL_MINOR = 2\nRHEL_MAJOR = 8\n']
            result = webhook.bugzilla.get_project_major_ver(project)
            self.assertEqual(result, '8')
            self.assertIn(("Unable to determine major version of project"
                           " 'kernel-test' from its name."), logs.output[-1])

        # Fail completely :(.
        with self.assertLogs('cki.webhook.bugzilla', level='ERROR') as logs:
            project.name = 'kernel-test'
            mock_get_file.side_effect = [None, b'RHEL_MINOR = 2\nRHEL_MEJOR = 8\n']
            result = webhook.bugzilla.get_project_major_ver(project)
            self.assertEqual(result, None)
            self.assertIn('Cannot find RHEL_MAJOR in Makefile.', logs.output[-1])

    @mock.patch('webhook.bugzilla.get_project_file')
    def test_get_mr_target_release(self, mock_get_file):
        project = mock.Mock()

        # Unable to determine major_ver, thus not able to get target
        project.name = 'kernel-test'
        mock_get_file.return_value = None
        result = webhook.bugzilla.get_mr_target_release(project, 'X.Y')
        self.assertEqual(result, None)

        # Target is not 'main', test zstream/alpha/beta branches.
        project.name = 'rhel-9'
        result = webhook.bugzilla.get_mr_target_release(project, '8.4')
        self.assertEqual(result, None)
        result = webhook.bugzilla.get_mr_target_release(project, '9.0-alpha')
        self.assertEqual(result, 'rhel-9.0.0-alpha')
        result = webhook.bugzilla.get_mr_target_release(project, '9.0-beta')
        self.assertEqual(result, 'rhel-9.0.0-beta')
        result = webhook.bugzilla.get_mr_target_release(project, '9.1-alpha')
        self.assertEqual(result, 'rhel-9.1.0')
        project.name = 'rhel-8'
        result = webhook.bugzilla.get_mr_target_release(project, '8.2')
        self.assertEqual(result, 'rhel-8.2.0')

        # Target is 'main' so return project.branches.list + 1.
        project.name = 'rhel-8'
        branch_list = []
        for branch in range(12):
            new_branch = mock.Mock()
            new_branch.name = f'8.{branch}'
            branch_list.append(new_branch)
        project.branches.list.return_value = branch_list
        result = webhook.bugzilla.get_mr_target_release(project, 'main')
        self.assertEqual(result, 'rhel-8.12.0')

        # The branch naming scheme is diferent before rhel8.
        project.name = 'rhel-7'
        branch_list = []
        for branch in range(9):
            new_branch = mock.Mock()
            new_branch.name = f'7.{branch}'
            branch_list.append(new_branch)
        project.branches.list.return_value = branch_list
        result = webhook.bugzilla.get_mr_target_release(project, 'main')
        self.assertEqual(result, 'rhel-7.9')
        project.name = 'rhel-7'
        result = webhook.bugzilla.get_mr_target_release(project, '7.9')
        self.assertEqual(result, 'rhel-7.9')
        result = webhook.bugzilla.get_mr_target_release(project, '7.3')
        self.assertEqual(result, 'rhel-7.3')
        project.name = 'rhel-6'
        result = webhook.bugzilla.get_mr_target_release(project, '6.10')
        self.assertEqual(result, 'rhel-6.10')
        project.name = 'rhel-alt-7'
        result = webhook.bugzilla.get_mr_target_release(project, 'main')
        self.assertEqual(result, 'rhel-alt-7.9')
        project.name = 'rhel-alt-7'
        result = webhook.bugzilla.get_mr_target_release(project, '7.4')
        self.assertEqual(result, 'rhel-alt-7.4')

    def test_bug_is_verified(self):
        bug_no = '12345678'
        bzcon = mock.Mock()
        bug_fields = webhook.bugzilla.BUG_FIELDS

        # getbug() failure.
        with self.assertLogs('cki.webhook.bugzilla', level='ERROR') as logs:
            bzcon.getbug.return_value = None
            self.assertFalse(webhook.bugzilla.bug_is_verified(bug_no, bzcon))
            bzcon.getbug.called_once_with(bug_no, bug_fields)
            self.assertIn('getbug() did not return a useful result for BZ 12345678.',
                          logs.output[-1])

        # getbug() returns an unverified bug.
        with self.assertLogs('cki.webhook.bugzilla', level='DEBUG') as logs:
            bug = mock.Mock(id=12345678, product='Red Hat Enterprise Linux 8', component='kernel',
                            cf_verified=[])
            bzcon.getbug.return_value = bug
            self.assertFalse(webhook.bugzilla.bug_is_verified(bug_no, bzcon))
            bzcon.getbug.called_once_with(bug_no, bug_fields)
            self.assertIn('Bug 12345678 is not Verified.', logs.output[-1])

        # getbug() returns an verified bug.
        with self.assertLogs('cki.webhook.bugzilla', level='DEBUG') as logs:
            bug = mock.Mock(id=12345678, product='Red Hat Enterprise Linux 8', component='kernel',
                            cf_verified=['Tested'])
            bzcon.getbug.return_value = bug
            self.assertTrue(webhook.bugzilla.bug_is_verified(bug_no, bzcon))
            bzcon.getbug.called_once_with(bug_no, bug_fields)
            self.assertIn('Bug 12345678 is Verified.', logs.output[-1])

    # pylint: disable=no-self-use
    @mock.patch('cki_lib.messagequeue.pika.BlockingConnection')
    @mock.patch('webhook.common.process_message')
    @mock.patch.dict(os.environ, {'BUGZILLA_ROUTING_KEYS': 'mocked', 'BUGZILLA_QUEUE': 'mocked'})
    def test_entrypoint(self, mocked_process, mocked_connection):
        """Check basic queue consumption."""

        # setup dummy consume data
        payload = {'foo': 'bar'}
        properties = mock.Mock()
        headers = {'header-key': 'value'}
        setattr(properties, 'headers', headers)
        consume_value = [(mock.Mock(), properties, json.dumps(payload))]
        mocked_connection().channel().consume.return_value = consume_value

        webhook.bugzilla.main([])

        mocked_process.assert_called()

    @mock.patch('gitlab.Gitlab')
    def test_unsupported_object_kind(self, mocked_gitlab):
        """Check handling an unsupported object kind."""
        payload = copy.deepcopy(self.PAYLOAD_MERGE)
        payload.update({'object_kind': 'foo'})

        self._test_payload(mocked_gitlab, False, payload, False)

    def test_validate_internal_bz(self):
        """Check for expected return value."""
        commits = {"deadb": ['redhat/Makefile']}
        self.assertTrue(webhook.bugzilla.validate_internal_bz(commits))
        commits = {"deadb": ['redhat/Makefile', '.gitlab-ci.yml']}
        self.assertTrue(webhook.bugzilla.validate_internal_bz(commits))
        commits = {"deadb": ['.gitlab/CODEOWNERS', '.gitlab-ci.yml']}
        self.assertTrue(webhook.bugzilla.validate_internal_bz(commits))
        commits['abcde'] = ['net/core/dev.c', 'include/net/netdevice.h']
        self.assertFalse(webhook.bugzilla.validate_internal_bz(commits))
        commits['abcde'] = ['net/core/dev.c', 'include/net/netdevice.h', 'redhat/configs/']
        self.assertFalse(webhook.bugzilla.validate_internal_bz(commits))

    @mock.patch('gitlab.Gitlab')
    def test_merge_request(self, mocked_gitlab):
        """Check handling of a merge request."""
        payload = copy.deepcopy(self.PAYLOAD_MERGE)
        payload['object_attributes']['target_branch'] = '8.3-net'
        self._test_payload(mocked_gitlab, True, payload, True)

    @mock.patch('gitlab.Gitlab')
    def test_note(self, mocked_gitlab):
        """Check handling of a note."""
        self._test_payload(mocked_gitlab, True, self.PAYLOAD_NOTE, False)

    @mock.patch('gitlab.Gitlab')
    def test_bz_re_evaluation1(self, mocked_gitlab):
        """Check handling of bz re-evaluation."""
        payload = copy.deepcopy(self.PAYLOAD_NOTE)
        payload["object_attributes"]["note"] = "request-evaluation"
        self._test_payload(mocked_gitlab, True, payload, True)

    @mock.patch('gitlab.Gitlab')
    def test_bz_re_evaluation2(self, mocked_gitlab):
        """Check handling of bz re-evaluation."""
        payload = copy.deepcopy(self.PAYLOAD_NOTE)
        payload["object_attributes"]["note"] = "request-bz-evaluation"
        self._test_payload(mocked_gitlab, True, payload, True)

    def _test_payload(self, mocked_gitlab, result, payload, assert_gitlab_called):
        with mock.patch('cki_lib.misc.is_production', return_value=True):
            self._test(mocked_gitlab, result, payload, assert_gitlab_called)
        with mock.patch('cki_lib.misc.is_production', return_value=False):
            self._test(mocked_gitlab, result, payload, assert_gitlab_called)

    # pylint: disable=too-many-arguments,too-many-locals
    @mock.patch('webhook.common.try_bugzilla_conn')
    def _test(self, mocked_gitlab, result, payload, assert_gitlab_called, mock_try_bugzilla):

        # setup dummy gitlab data
        project = mock.Mock(name_with_namespace="foo.bar",
                            namespace={'name': '8.y'})
        project.name = 'rhel-8'
        target = "main"
        if payload['object_kind'] == 'merge_request':
            target = payload['object_attributes']['target_branch']
        if payload['object_kind'] == 'note':
            target = payload['merge_request']['target_branch']
        merge_request = mock.MagicMock(target_branch=target)
        c1 = mock.Mock(id="1234",
                       message="1\n"
                       "Bugzilla: http://bugzilla.redhat.com/show_bug.cgi?id=1234",
                       parent_ids=["abcd"])
        c2 = mock.Mock(id="4567",
                       message="2\nBugzilla: http://bugzilla.test?id=45678\n"
                       "Bugzilla: https://bugzilla.redhat.com/4567\n"
                       "Depends: http://bugzilla.redhat.com/1234",
                       parent_ids=["1234"])
        c3 = mock.Mock(id="890a",
                       message="3\nBugzilla: INTERNAL",
                       parent_ids=["face"])
        c3.diff = mock.Mock(return_value=[{'new_path': 'redhat/Makefile'}])
        c4 = mock.Mock(id="face",
                       message="This is a merge",
                       parent_ids=["abcd", "4567"])
        c5 = mock.Mock(id="deadbeef",
                       message="5\nBugzilla: 565472, 7784378\n"
                       "Nothing to see here\n"
                       "Bugzilla: http://bugzilla.redhat.com/1234567\n"
                       "Bugzilla: https://bugzilla.redhat.com/7654321\n"
                       "Bugzilla: http://bugzilla.redhat.com/1989898",
                       parent_ids=["890a"])
        c6 = mock.Mock(id="1800555",
                       message="6\nBugzilla: http://bugzilla.redhat.com/show_bug.cgi?id=1800555",
                       parent_ids=["deadbeef"])
        c7 = mock.Mock(id="e404",
                       message="7\n"
                       "https://bugzilla.redhat.com/show_bug.cgi?id=9983023",
                       parent_ids=["1800555"])
        c8 = mock.Mock(id="abcd",
                       message="8\n"
                       "Depends: "
                       "https://bugzilla.redhat.com/show_bug.cgi?id=7654321\n",
                       parent_ids=["1800555"])
        mocked_gitlab().__enter__().projects.get.return_value = project
        project.mergerequests.get.return_value = merge_request
        project.labels.list.return_value = []
        merge_request.iid = 2
        merge_request.labels = []
        merge_request.commits.return_value = [c1, c2, c3, c4, c5, c6, c7, c8]
        merge_request.pipeline = {'status': 'success'}
        branch = mock.Mock()
        branch.configure_mock(name="8.2")
        project.branches.list.return_value = [branch]
        project.commits = {"1234": c1, "4567": c2, "890a": c3, "face": c4,
                           "deadbeef": c5, "1800555": c6, "e404": c7,
                           "abcd": c8}
        self.assertEqual(webhook.common.extract_bzs(c1.message), ["1234"])
        self.assertEqual(webhook.common.extract_dependencies(c1.message), [])
        self.assertEqual(webhook.common.extract_bzs(c2.message), ["4567"])
        self.assertEqual(webhook.common.extract_dependencies(c2.message),
                         ["1234"])
        self.assertEqual(webhook.common.extract_bzs(c3.message), ["INTERNAL"])
        self.assertEqual(webhook.common.extract_bzs(c5.message),
                         ["1234567", "7654321", "1989898"])
        self.assertEqual(webhook.common.extract_bzs(c6.message), ["1800555"])
        self.assertEqual(webhook.common.extract_bzs(c7.message), [])
        self.assertEqual(webhook.common.extract_dependencies(c8.message),
                         ["7654321"])

        # setup dummy post results data
        presult = mock.Mock()
        presult.json.return_value = {
            "error": "*** No approved issue IDs referenced in log message or"
                     " changelog for HEAD\n*** Unapproved issue IDs referenced"
                     " in log message or changelog for HEAD\n*** Commit HEAD"
                     " denied\n*** Current checkin policy requires:\n"
                     "    release == +\n"
                     "*** See https://rules.doc/doc for more information",
            "result": "fail",
            "logs": "*** Checking commit HEAD\n*** Resolves:\n***   "
                    "Unapproved:\n***     rhbz#1234 (release?, qa_ack?, "
                    "devel_ack?, mirror+)\n"
        }

        with mock.patch('webhook.bugzilla.SESSION.post', return_value=presult):
            with mock.patch('webhook.bugzilla.get_mr_target_release', return_value='rhel-8.2.0'):
                return_value = \
                    webhook.common.process_message('dummy', payload,
                                                   webhook.bugzilla.WEBHOOKS, False)

        if result:
            mocked_gitlab.assert_called_with(
                'https://web.url', private_token='TOKEN',
                session=mock.ANY)

            self.assertTrue(return_value)
            if assert_gitlab_called:
                mocked_gitlab().__enter__().projects.get.assert_called_with(1)
                project.mergerequests.get.assert_called_with(2)
            else:
                mocked_gitlab().__enter__().projects.get.assert_not_called()
                project.mergerequests.get.assert_not_called()
        else:
            self.assertFalse(return_value)
