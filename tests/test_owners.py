"""Owners tests."""
import unittest

import webhook.owners


class TestOwners(unittest.TestCase):
    def test_empty_parser(self):
        parser = webhook.owners.Parser(None)
        self.assertEqual(len(parser.get_all_entries()), 0)

        parser = webhook.owners.Parser("")
        self.assertEqual(len(parser.get_all_entries()), 0)

        parser = webhook.owners.Parser(("subsystems:\n"
                                        " - invalid: Does Not Match\n"))
        entries = parser.get_all_entries()
        self.assertEqual(len(entries), 1)

        entry = entries[0]
        self.assertEqual(entry.get_subsystem_name(), None)
        self.assertEqual(entry.get_subsystem_label(), None)
        self.assertEqual(entry.get_ready_for_merge_label_deps(), [])
        self.assertEqual(entry.get_status(), None)
        self.assertEqual(entry.get_maintainers(), [])
        self.assertEqual(entry.get_reviewers(), [])
        self.assertEqual(entry.get_scm(), None)
        self.assertEqual(entry.get_mailing_list(), None)
        self.assertEqual(entry.get_path_include_regexes(), [])
        self.assertEqual(entry.get_path_includes(), [])
        self.assertEqual(entry.get_path_excludes(), [])
        self.assertFalse(entry.matches(['makefile']))

    def test_parser(self):
        owners_yaml = ("subsystems:\n"
                       " - subsystem: Some Subsystem\n"
                       "   labels:\n"
                       "     name: redhat\n"
                       "     readyForMergeDeps:\n"
                       "       - testDep\n"
                       "   status: Maintained\n"
                       "   maintainers:\n"
                       "     - name: User 1\n"
                       "       email: user1@redhat.com\n"
                       "     - name: User 2\n"
                       "       email: user2@redhat.com\n"
                       "   reviewers:\n"
                       "     - name: User 3\n"
                       "       email: user3@redhat.com\n"
                       "     - name: User 4\n"
                       "       email: user4@redhat.com\n"
                       "   paths:\n"
                       "       includes:\n"
                       "          - makefile\n"
                       "          - redhat/\n"
                       "       includeRegexes:\n"
                       "          - bpf\n"
                       "       excludes:\n"
                       "          - redhat/configs/\n"
                       "   scm: git https://gitlab.com/cki-project/kernel-ark.git\n"
                       "   mailingList: https://somelist.redhat.com/xxx/\n")
        parser = webhook.owners.Parser(owners_yaml)
        entries = parser.get_all_entries()
        self.assertEqual(len(entries), 1)

        entry = entries[0]
        self.assertEqual(entry.get_subsystem_name(), 'Some Subsystem')
        self.assertEqual(entry.get_subsystem_label(), 'redhat')
        self.assertEqual(entry.get_ready_for_merge_label_deps(), ['testDep'])
        self.assertEqual(entry.get_status(), 'Maintained')
        self.assertEqual(entry.get_maintainers(), [{'name': 'User 1', 'email': 'user1@redhat.com'},
                                                   {'name': 'User 2', 'email': 'user2@redhat.com'}])
        self.assertEqual(entry.get_reviewers(), [{'name': 'User 3', 'email': 'user3@redhat.com'},
                                                 {'name': 'User 4', 'email': 'user4@redhat.com'}])
        self.assertEqual(entry.get_scm(), 'git https://gitlab.com/cki-project/kernel-ark.git')
        self.assertEqual(entry.get_mailing_list(), 'https://somelist.redhat.com/xxx/')
        self.assertEqual(entry.get_path_include_regexes(), ['bpf'])
        self.assertEqual(entry.get_path_includes(), ['makefile', 'redhat/'])
        self.assertEqual(entry.get_path_excludes(), ['redhat/configs/'])
        self.assertEqual(str(entry), 'Some Subsystem')

        self.assertTrue(entry.matches(['makefile']))
        self.assertTrue(entry.matches(['redhat/']))
        self.assertTrue(entry.matches(['redhat/Makefile']))
        self.assertTrue(entry.matches(['include/linux/bpf.h']))
        self.assertFalse(entry.matches(['redhat/configs/generic/CONFIG_NET']))
        self.assertFalse(entry.matches(['drivers/iio/light/tsl2772.c']))
        self.assertTrue(entry.matches(['makefile', 'redhat/configs/generic/CONFIG_NET']))

        self.assertEqual(len(parser.get_matching_entries(['makefile'])), 1)
        self.assertEqual(len(parser.get_matching_entries(['redhat/'])), 1)
        self.assertEqual(len(parser.get_matching_entries(['redhat/Makefile'])), 1)
        self.assertEqual(len(parser.get_matching_entries(['redhat/configs/generic/CONFIG_NET'])), 0)
        self.assertEqual(len(parser.get_matching_entries(['drivers/iio/light/tsl2772.c'])), 0)

    def test_glob_match(self):
        result = webhook.owners.glob_match('usr/bin/false', 'usr/*')
        self.assertEqual(result, False)
        result = webhook.owners.glob_match('usr/bin/false', 'usr/bin/*')
        self.assertEqual(result, True)
        result = webhook.owners.glob_match('usr/bin/false', 'usr/bin/f[ab]*')
        self.assertEqual(result, True)
        result = webhook.owners.glob_match('usr/bin/false', 'usr/bin/f[xy]*')
        self.assertEqual(result, False)
        result = webhook.owners.glob_match('usr/bin/false', 'usr/bin/*al*')
        self.assertEqual(result, True)
        result = webhook.owners.glob_match('usr/bin/false', 'usr/bin/false')
        self.assertEqual(result, True)
        result = webhook.owners.glob_match('usr/bin/false', 'usr/*/false')
        self.assertEqual(result, True)
        result = webhook.owners.glob_match('usr/bin/false', 'usr/???/false')
        self.assertEqual(result, True)
        result = webhook.owners.glob_match('usr/bin/false', 'usr/????/false')
        self.assertEqual(result, False)
        result = webhook.owners.glob_match('usr/bin/false', '*')
        self.assertEqual(result, False)
        result = webhook.owners.glob_match('usr/bin/false', '*/*')
        self.assertEqual(result, False)
        result = webhook.owners.glob_match('usr/bin/false', '*/*/*')
        self.assertEqual(result, True)
        result = webhook.owners.glob_match('usr/bin/false', 'usr/')
        self.assertEqual(result, True)
        result = webhook.owners.glob_match('usr/bin/false', 'usr/bin/')
        self.assertEqual(result, True)
        result = webhook.owners.glob_match('usr/bin/false', 'usr/sbin/')
        self.assertEqual(result, False)
        result = webhook.owners.glob_match('usr/bin/false', '*/')
        self.assertEqual(result, True)
        result = webhook.owners.glob_match('usr/bin/false', 'usr/bin')
        self.assertEqual(result, False)
