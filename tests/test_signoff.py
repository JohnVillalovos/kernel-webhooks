"""Webhook interaction tests."""
import copy
import json
import os
import unittest
from unittest import mock

import webhook.common
import webhook.signoff


@mock.patch('cki_lib.gitlab.get_token', mock.Mock(return_value='TOKEN'))
class TestSignoff(unittest.TestCase):
    """ Test Webhook class."""

    PAYLOAD_MERGE = {'object_kind': 'merge_request',
                     'project': {'id': 1,
                                 'web_url': 'https://web.url/g/p'},
                     'object_attributes': {'iid': 2,
                                           'action': 'update',
                                           'oldrev': 'b0359d3066'},
                     'changes': {'labels': {'previous': [],
                                            'current': []}},
                     'state': 'opened'
                     }

    # OK commits
    c1 = mock.Mock(id="c1",
                   author_name="John Doe",
                   author_email="jdoe@redhat.com",
                   message="1\n"
                   "Signed-off-by: John Doe <jdoe@redhat.com>\n"
                   "Signed-off-by: Jane Doe <jadoe@redhat.com>",
                   parent_ids=["abcd"])
    c2 = mock.Mock(id="c2",
                   author_name="Jane Doe",
                   author_email="jadoe@redhat.com",
                   message="2\n"
                   "Signed-off-by: John Doe <jdoe@redhat.com>\n"
                   "Signed-off-by: Jane Doe <jadoe@redhat.com>",
                   parent_ids=["1234"])
    # NOMATCH commits
    c3 = mock.Mock(id="c3",
                   author_name="John Dough",
                   author_email="jdough@redhat.com",
                   message="3\n"
                   "Signed-off-by: John Doe <jdoe@redhat.com>\n"
                   "    Signed-off-by: John Dough <jdough@redhat.com>\n"
                   "Signed-off-by: Jane Doe <jadoe@redhat.com>",
                   parent_ids=["4321"])
    c4 = mock.Mock(id="c4",
                   author_name="John Doe",
                   author_email="jdoe@redhat.com",
                   message="4\n"
                   "Signed-off-by: John Doe <john@redhat.com>\n",
                   parent_ids=["5678"])
    c5 = mock.Mock(id="c5",
                   author_name="John Doe",
                   author_email="jdoe@redhat.com",
                   message="5\n"
                   "Signed-off-by: Jon Doe <jdoe@redhat.com>\n",
                   parent_ids=["12345"])
    # MISSING commit
    c6 = mock.Mock(id="c6",
                   author_name="John Doe",
                   author_email="jdoe@redhat.com",
                   message="6\n"
                   "My cool commit.",
                   parent_ids=["54321"])
    # Merge commit (mutliple parents)
    c7 = mock.Mock(id="c7",
                   author_name="John Doe",
                   author_email="jdoe@redhat.com",
                   message="This is a merge",
                   parent_ids=["abcd", "4567"])

    commit_results = {c1: webhook.signoff.State.OK,
                      c2: webhook.signoff.State.OK,
                      c3: webhook.signoff.State.NOMATCH,
                      c4: webhook.signoff.State.NOMATCH,
                      c5: webhook.signoff.State.NOMATCH,
                      c6: webhook.signoff.State.MISSING}

    def test_get_current_signoff_scope(self):
        """Check get_current_signoff_scope() returns the expected scope."""
        self.assertEqual(webhook.signoff.get_current_signoff_scope(['Signoff::OK']), "OK")
        self.assertEqual(webhook.signoff.get_current_signoff_scope(['Signoff::NeedsReview']),
                         "NeedsReview")
        self.assertEqual(webhook.signoff.get_current_signoff_scope(['']), None)
        self.assertEqual(webhook.signoff.get_current_signoff_scope(['noise', 'fake']), None)

    def test_find_dco_ok(self):
        """Check find_dco() function returns OK value."""
        for commit in self.commit_results:
            if self.commit_results[commit] == webhook.signoff.State.OK:
                self.assertEqual(webhook.signoff.find_dco(commit), webhook.signoff.State.OK)

    def test_find_dco_nomatch(self):
        """Check find_dco() function returns NOMATCH value."""
        for commit in self.commit_results:
            if self.commit_results[commit] == webhook.signoff.State.NOMATCH:
                self.assertEqual(webhook.signoff.find_dco(commit), webhook.signoff.State.NOMATCH)

    def test_find_dco_missing(self):
        """Check find_dco() function returns MISSING value."""
        for commit in self.commit_results:
            if self.commit_results[commit] == webhook.signoff.State.MISSING:
                self.assertEqual(webhook.signoff.find_dco(commit), webhook.signoff.State.MISSING)

    def test_process_commits(self):
        """Check function returns expected dict contents."""
        project = mock.Mock()
        project.commits = {}
        expected_return = {}
        for commit in self.commit_results:
            project.commits[commit.id] = commit
            expected_return[commit.id] = self.commit_results[commit]

        return_value = webhook.signoff.process_commits(project, list(project.commits.values()))
        self.assertEqual(return_value, expected_return)

    def test_generate_table_fail(self):
        """Check the function returns a string with the expected output."""
        fail_return = ("**DCO Signoff Error(s)!**\n\n"
                       "%s"
                       "| **Commit** | **Signoff Status** |\n"
                       "|----|----|\n"
                       "| c1 | OK |\n"
                       "| c2 | OK |\n"
                       "| c3 | **NOMATCH** [^2] |\n"
                       "| c4 | **NOMATCH** [^2] |\n"
                       "| c5 | **NOMATCH** [^2] |\n"
                       "| c6 | **MISSING** [^1] |\n"
                       "[^1]: %s\n"
                       "[^2]: %s\n"
                       % (webhook.signoff.DCO_FAIL,
                          webhook.signoff.footnotes[webhook.signoff.State.MISSING],
                          webhook.signoff.footnotes[webhook.signoff.State.NOMATCH]))

        commits = {}
        for commit in self.commit_results:
            commits[commit.id] = self.commit_results[commit]
        return_value = webhook.signoff.generate_table(commits)
        # pylint: disable=invalid-name
        self.maxDiff = None
        self.assertEqual(return_value, fail_return)

    def test_generate_table_pass(self):
        """Check the function returns a string with the expected output."""
        dco_string = webhook.signoff.DCO_PASS % (2)
        pass_return = ("**DCO Signoff Report**\n\n"
                       "%s"
                       "| **Commit** | **Signoff Status** |\n"
                       "|----|----|\n"
                       "| c1 | OK |\n"
                       "| c2 | OK |\n"
                       "[^1]: %s\n"
                       "[^2]: %s\n"
                       % (dco_string,
                          webhook.signoff.footnotes[webhook.signoff.State.MISSING],
                          webhook.signoff.footnotes[webhook.signoff.State.NOMATCH]))

        commits = {}
        for commit in self.commit_results:
            if self.commit_results[commit] == webhook.signoff.State.OK:
                commits[commit.id] = self.commit_results[commit]
        return_value = webhook.signoff.generate_table(commits)
        self.maxDiff = None
        self.assertEqual(return_value, pass_return)

    def test_update_mr(self):
        """Check for expected debug output and calls."""
        gl_project = mock.Mock()
        gl_project.labels.list.return_value = []
        merge_request = mock.Mock()
        gl_project.mergerequests.get.return_value = merge_request
        gl_project.labels.list.return_value = []
        merge_request.notes.create.return_value = True
        # Test DCO check failure with no existing label.
        with self.assertLogs('cki.webhook.signoff', level='DEBUG') as logs:
            merge_request.labels = []
            results_table = "FAKE TABLE FAILED\n"
            webhook.signoff.update_mr(gl_project, merge_request, results_table, False)
            self.assertIn("current scope: 'None', new scope: 'NeedsReview'", logs[-1][0])
        # Test DCO check success with no existing label.
        with self.assertLogs('cki.webhook.signoff', level='DEBUG') as logs:
            merge_request.labels = []
            results_table = "FAKE TABLE PASSED\n"
            webhook.signoff.update_mr(gl_project, merge_request, results_table, False)
            self.assertIn("current scope: 'None', new scope: 'OK'", logs[-1][0])
        # Test DCO check success with existing OK label.
        with self.assertLogs('cki.webhook.signoff', level='DEBUG') as logs:
            merge_request.labels = ['Signoff::OK']
            results_table = "FAKE TABLE PASSED\n"
            webhook.signoff.update_mr(gl_project, merge_request, results_table, False)
            self.assertIn("current scope: 'OK', new scope: 'OK'", logs[-1][0])

    # pylint: disable=no-self-use
    @mock.patch('cki_lib.messagequeue.pika.BlockingConnection')
    @mock.patch('webhook.common.process_message')
    @mock.patch.dict(os.environ, {'SIGNOFF_ROUTING_KEYS': 'mocked', 'SIGNOFF_QUEUE': 'mocked'})
    def test_entrypoint(self, mocked_process, mocked_connection):
        """Check basic queue consumption."""

        # setup dummy consume data
        payload = {'foo': 'bar'}
        properties = mock.Mock()
        headers = {'header-key': 'value'}
        setattr(properties, 'headers', headers)
        consume_value = [(mock.Mock(), properties, json.dumps(payload))]
        mocked_connection().channel().consume.return_value = consume_value

        webhook.signoff.main([])
        mocked_process.assert_called()

    @mock.patch('gitlab.Gitlab')
    def test_good_merge_request(self, mocked_gitlab):
        """Check handling of a merge request with good signoffs."""
        commits = [self.c1, self.c2]
        self._test_payload(mocked_gitlab, True, self.PAYLOAD_MERGE, commits)

    @mock.patch('gitlab.Gitlab')
    def test_bad_merge_request(self, mocked_gitlab):
        """Check handling of a merge request with bad signoffs."""
        commits = [self.c1, self.c2, self.c3, self.c4, self.c5, self.c6, self.c7]
        self._test_payload(mocked_gitlab, True, self.PAYLOAD_MERGE, commits)

    @mock.patch('gitlab.Gitlab')
    def test_mr_with_bad_action(self, mocked_gitlab):
        """Check handling of a merge request with unwanted actions."""
        commits = [self.c1, self.c2]
        payload = copy.deepcopy(self.PAYLOAD_MERGE)
        del payload['object_attributes']['oldrev']
        self._test_payload(mocked_gitlab, False, payload, commits)
        payload['object_attributes']['action'] = 'close'
        self._test_payload(mocked_gitlab, False, payload, commits)

    def _test_payload(self, mocked_gitlab, result, payload, commits):
        with mock.patch('cki_lib.misc.is_production', return_value=True):
            self._test(mocked_gitlab, result, payload, commits)
        with mock.patch('cki_lib.misc.is_production', return_value=False):
            self._test(mocked_gitlab, result, payload, commits)

    def _test(self, mocked_gitlab, result, payload, commits):
        project = mock.Mock()
        merge_request = mock.Mock()
        mocked_gitlab().__enter__().projects.get.return_value = project
        project.mergerequests.get.return_value = merge_request
        project.labels.list.return_value = []
        merge_request.iid = 2
        merge_request.labels = []
        merge_request.commits.return_value = commits
        project.commits = {}
        for commit in commits:
            project.commits[commit.id] = commit

        if result:
            return_value = webhook.common.process_message('dummy', payload,
                                                          webhook.signoff.WEBHOOKS, False)
            mocked_gitlab.assert_called_with(
                'https://web.url', private_token='TOKEN',
                session=mock.ANY)
            mocked_gitlab().__enter__().projects.get.assert_called_with(1)
            project.mergerequests.get.assert_called_with(2)
            self.assertTrue(return_value)
        else:
            webhook.common.process_message('dummy', payload, webhook.signoff.WEBHOOKS, False)
            mocked_gitlab().__enter__().projects.get.assert_not_called()

    @mock.patch('webhook.signoff._do_process_mr')
    def test_process_mr_no_label_changes(self, mock_process_mr):
        msg = mock.Mock()
        msg.payload = {'object_kind': 'merge_request',
                       'project': {'id': 1,
                                   'web_url': 'https://web.url/g/p'},
                       'object_attributes': {'iid': 2,
                                             'action': 'update'},
                       'changes': {'labels': {'previous': [{'title': 'Signoff::OK'}],
                                              'current': [{'title': 'Signoff::OK'}]}}
                       }
        webhook.signoff.process_mr(None, msg)
        mock_process_mr.assert_not_called()

    @mock.patch('webhook.signoff._do_process_mr')
    def test_process_mr_signoff_label_changed(self, mock_process_mr):
        msg = mock.Mock()
        msg.payload = {'object_kind': 'merge_request',
                       'project': {'id': 1,
                                   'web_url': 'https://web.url/g/p'},
                       'object_attributes': {'iid': 2,
                                             'action': 'update'},
                       'changes': {'labels': {'previous': [{'title': 'Signoff::Missing'},
                                                           {'title': 'somethingelse::OK'}],
                                              'current': [{'title': 'Signoff::OK'},
                                                          {'title': 'somethingelse::OK'}]}}
                       }
        webhook.signoff.process_mr(None, msg)
        mock_process_mr.assert_called_with(None, msg, 2, True)

    @mock.patch('webhook.signoff._do_process_mr')
    def test_process_mr_signoff_label_removed(self, mock_process_mr):
        msg = mock.Mock()
        msg.payload = {'object_kind': 'merge_request',
                       'project': {'id': 1,
                                   'web_url': 'https://web.url/g/p'},
                       'object_attributes': {'iid': 2,
                                             'action': 'update'},
                       'changes': {'labels': {'previous': [{'title': 'Signoff::OK'},
                                                           {'title': 'somethingelse::OK'}],
                                              'current': [{'title': 'somethingelse::OK'}]}}
                       }
        webhook.signoff.process_mr(None, msg)
        mock_process_mr.assert_called_with(None, msg, 2, True)

    @mock.patch('webhook.signoff._do_process_mr')
    def test_process_mr_signoff_label_added(self, mock_process_mr):
        msg = mock.Mock()
        msg.payload = {'object_kind': 'merge_request',
                       'project': {'id': 1,
                                   'web_url': 'https://web.url/g/p'},
                       'object_attributes': {'iid': 2,
                                             'action': 'update'},
                       'changes': {'labels': {'previous': [{'title': 'somethingelse::OK'}],
                                              'current': [{'title': 'Signoff::OK'},
                                                          {'title': 'somethingelse::OK'}]}}
                       }
        webhook.signoff.process_mr(None, msg)
        mock_process_mr.assert_called_with(None, msg, 2, True)

    @mock.patch('webhook.signoff._do_process_mr')
    def test_process_note_comment1(self, mock_process_mr):
        msg = mock.Mock()
        msg.payload = {'object_kind': 'note',
                       'project': {'id': 1,
                                   'web_url': 'https://web.url/g/p'},
                       'object_attributes': {'note': 'some comment',
                                             'noteable_type': 'MergeRequest'},
                       'merge_request': {'iid': 2}}
        webhook.signoff.process_note(None, msg)
        mock_process_mr.assert_not_called()

    @mock.patch('webhook.signoff._do_process_mr')
    def test_process_note_comment2(self, mock_process_mr):
        msg = mock.Mock()
        msg.payload = {'object_kind': 'note',
                       'project': {'id': 1,
                                   'web_url': 'https://web.url/g/p'},
                       'object_attributes': {'note': 'request-signoff-evaluation',
                                             'noteable_type': 'MergeRequest'},
                       'merge_request': {'iid': 2}}
        webhook.signoff.process_note(None, msg)
        mock_process_mr.assert_called_with(None, msg, 2, True)
