"""Webhook interaction tests."""
import unittest
from unittest import mock

import webhook.common
import webhook.subsystems


@mock.patch('cki_lib.gitlab.get_token', mock.Mock(return_value='TOKEN'))
class TestSubsystems(unittest.TestCase):

    MAPPINGS = {'subsystems':
                [{'subsystem': 'MEMORY MANAGEMENT',
                  'labels': {'name': 'mm'},
                  'paths': {'includes': ['include/linux/mm.h', 'include/linux/vmalloc.h', 'mm/']}
                  },
                 {'subsystem': 'NETWORKING',
                  'labels': {'name': 'net',
                             'readyForMergeDeps': ['lnst']},
                  'paths': {'includes': ['include/linux/net.h', 'include/net/', 'net/']},
                  },
                 {'subsystem': 'XDP',
                  'labels': {'name': 'xdp'},
                  'paths': {'includes': ['kernel/bpf/devmap.c', 'include/net/page_pool.h'],
                            'includeRegexes': ['xdp']}
                  }]
                }

    OWNERS_YAML = ("subsystems:\n"
                   " - subsystem: MEMORY MANAGEMENT\n"
                   "   labels:\n"
                   "     name: mm\n"
                   "   paths:\n"
                   "       includes:\n"
                   "          - include/linux/mm.h\n"
                   "          - include/linux/vmalloc.h\n"
                   "          - mm/\n"
                   " - subsystem: NETWORKING\n"
                   "   labels:\n"
                   "     name: net\n"
                   "     readyForMergeDeps:\n"
                   "       - lnst\n"
                   "   paths:\n"
                   "       includes:\n"
                   "          - include/linux/net.h\n"
                   "          - include/net/\n"
                   "          - net/\n"
                   " - subsystem: XDP\n"
                   "   labels:\n"
                   "     name: xdp\n"
                   "   paths:\n"
                   "       includes:\n"
                   "          - kernel/bpf/devmap.c\n"
                   "          - include/net/page_pool.h\n"
                   "       includeRegexes:\n"
                   "          - xdp\n")

    FILE_CONTENTS = (
        '---\n'
        'subsystems:\n'
        ' - subsystem: MEMORY MANAGEMENT\n'
        '   labels:\n'
        '     name: mm\n'
        '   paths:\n'
        '       includes:\n'
        '          - include/linux/mm.h\n'
        '          - include/linux/vmalloc.h\n'
        '          - mm/\n'
        ' - subsystem: NETWORKING\n'
        '   labels:\n'
        '     name: net\n'
        '     readyForMergeDeps:\n'
        '       - lnst\n'
        '   paths:\n'
        '       includes:\n'
        '          - include/linux/net.h\n'
        '          - include/net/\n'
        '          - net/\n'
        ' - subsystem: XDP\n'
        '   labels:\n'
        '     name: xdp\n'
        '   paths:\n'
        '       includes:\n'
        '          - kernel/bpf/devmap.c\n'
        '          - include/net/page_pool.h\n'
        '       includeRegexes:\n'
        '          - xdp\n'
        )

    CHANGES = {'changes': [{'old_path': 'drivers/net/ethernet/intel/ixgbe/ixgbe.h',
                            'new_path': 'drivers/net/ethernet/intel/ixgbe/ixgbe.h'},
                           {'old_path': 'include/linux/mm.h',
                            'new_path': 'include/linux/mm2.h'},
                           {'old_path': 'include/net/bonding.h',
                            'new_path': 'include/net/bond_xor.h'}]
               }

    def test_load_yaml_data(self):
        with self.assertLogs('cki.webhook.subsystems', level='ERROR') as logs:
            result = webhook.subsystems.load_yaml_data('bad_map_file.yml')
            self.assertEqual(result, None)
            self.assertIn("No such file or directory: 'bad_map_file.yml'", logs.output[-1])

        with mock.patch('builtins.open', mock.mock_open(read_data=self.FILE_CONTENTS)):
            result = webhook.subsystems.load_yaml_data('map_file.yml')
            self.assertEqual(result, self.MAPPINGS)

    def test_user_wants_notification(self):
        user_data = {'all': ['net/'],
                     '8.y': ['include/net/bonding.h'],
                     '8.2': ['include/net/*'],
                     '8.1': ['*/net/*']}

        path_list = ['net/core/dev.c']
        self.assertTrue(webhook.subsystems.user_wants_notification(user_data, path_list, '8.y'))
        self.assertTrue(webhook.subsystems.user_wants_notification(user_data, path_list, '8.2'))
        self.assertTrue(webhook.subsystems.user_wants_notification(user_data, path_list, '8.1'))

        path_list = ['networking/ipv8.c']
        self.assertFalse(webhook.subsystems.user_wants_notification(user_data, path_list, '8.y'))
        self.assertFalse(webhook.subsystems.user_wants_notification(user_data, path_list, '8.2'))
        self.assertFalse(webhook.subsystems.user_wants_notification(user_data, path_list, '8.1'))

        path_list = ['include/net/bond_3ad.h']
        self.assertFalse(webhook.subsystems.user_wants_notification(user_data, path_list, '8.y'))
        self.assertTrue(webhook.subsystems.user_wants_notification(user_data, path_list, '8.2'))
        self.assertTrue(webhook.subsystems.user_wants_notification(user_data, path_list, '8.1'))

        path_list = ['include/net/bonding.h']
        self.assertTrue(webhook.subsystems.user_wants_notification(user_data, path_list, '8.y'))
        self.assertTrue(webhook.subsystems.user_wants_notification(user_data, path_list, '8.2'))
        self.assertTrue(webhook.subsystems.user_wants_notification(user_data, path_list, '8.1'))

        path_list = ['include/net/bonding/bonding.h']
        self.assertFalse(webhook.subsystems.user_wants_notification(user_data, path_list, '8.y'))
        self.assertFalse(webhook.subsystems.user_wants_notification(user_data, path_list, '8.2'))
        self.assertFalse(webhook.subsystems.user_wants_notification(user_data, path_list, '8.1'))

        path_list = ['include/net/bonding/bonding.h']
        self.assertFalse(webhook.subsystems.user_wants_notification(user_data, path_list, '8.y'))
        self.assertFalse(webhook.subsystems.user_wants_notification(user_data, path_list, '8.2'))
        self.assertFalse(webhook.subsystems.user_wants_notification(user_data, path_list, '8.1'))

    @mock.patch('os.listdir')
    @mock.patch('webhook.subsystems.load_yaml_data')
    @mock.patch('webhook.subsystems.user_wants_notification')
    def test_do_usermapping(self, mock_user_wants, mock_loader, mock_listdir):
        # Cannot get file listing.
        with self.assertLogs('cki.webhook.subsystems', level='ERROR') as logs:
            mock_listdir.side_effect = PermissionError
            result = webhook.subsystems.do_usermapping('8.y', [], '/repo')
            self.assertEqual(result, [])
            self.assertIn('Problem listing path', logs.output[-1])
            mock_user_wants.assert_not_called()

        # Error loading user data.
        with self.assertLogs('cki.webhook.subsystems', level='ERROR') as logs:
            mock_listdir.side_effect = None
            mock_listdir.return_value = ['user1']
            mock_loader.side_effect = [None]
            result = webhook.subsystems.do_usermapping('8.y', [], '/repo')
            self.assertEqual(result, [])
            self.assertIn("Error loading user data from path '/repo/users/user1'.",
                          logs.output[-1])
            mock_user_wants.assert_not_called()

        mock_listdir.return_value = ['user1', 'user2', 'user3']
        mock_loader.side_effect = None
        path_list = ['include/net/bonding.h', 'net/core/dev.c']

        user1_data = {'all': ['include/net/bonding.h']}
        user2_data = {'all': ['drivers/net/bonding/']}
        user3_data = {'all': ['include/net/bonding.h']}
        mock_loader.side_effect = [user1_data, user2_data, user3_data]
        mock_user_wants.side_effect = [True, False, True]
        result = webhook.subsystems.do_usermapping('8.y', path_list, '/repo')
        call_list = [mock.call(user1_data, path_list, '8.y'),
                     mock.call(user2_data, path_list, '8.y'),
                     mock.call(user3_data, path_list, '8.y')]

        self.assertEqual(mock_user_wants.call_count, 3)
        mock_user_wants.assert_has_calls(call_list, any_order=True)
        self.assertEqual(sorted(result), ['user1', 'user3'])

    @mock.patch('cki_lib.misc.is_production', mock.Mock(return_value=True))
    def test_post_notifications(self):
        mock_mr = mock.Mock()
        mock_mr.iid = 2
        mock_mr.participants.return_value = [{'username': 'user1'},
                                             {'username': 'user2'},
                                             {'username': 'user3'}]

        # No one new to notify.
        with self.assertLogs('cki.webhook.subsystems', level='INFO') as logs:
            user_list = ['user1', 'user3']
            webhook.subsystems.post_notifications(mock_mr, user_list, 'https://gitlab.com/project1')
            self.assertIn('No one new to notify.', logs.output[-1])
            mock_mr.participants.assert_called()
            mock_mr.notes.create.assert_not_called()

        # Create a note.
        with self.assertLogs('cki.webhook.subsystems', level='INFO') as logs:
            user_list = ['user1', 'user2', 'user4', 'user5']
            template = webhook.subsystems.NOTIFICATION_TEMPLATE
            note_text = template.format(header=webhook.subsystems.NOTIFICATION_HEADER,
                                        users='@user4 @user5',
                                        project='https://gitlab.com/project1')
            webhook.subsystems.post_notifications(mock_mr, user_list, 'https://gitlab.com/project1')
            self.assertIn('Posting notification on MR 2:', logs.output[-1])
            mock_mr.participants.assert_called()
            mock_mr.notes.create.assert_called_with({'body': note_text})

    def test_make_labels(self):
        topics = ['driver ixgbe', 'include', 'net', 'lnst::NeedsTesting']
        expected_list = [{'name': f'{webhook.subsystems.DRIVER_LABEL_PREFIX}:ixgbe',
                          'color': webhook.subsystems.DRIVER_LABEL_COLOR,
                          'description': webhook.subsystems.DRIVER_LABEL_DESC % 'ixgbe'},
                         {'name': f'{webhook.subsystems.SUBSYS_LABEL_PREFIX}:include',
                          'color': webhook.subsystems.SUBSYS_LABEL_COLOR,
                          'description': webhook.subsystems.SUBSYS_LABEL_DESC % 'include'},
                         {'name': f'{webhook.subsystems.SUBSYS_LABEL_PREFIX}:net',
                          'color': webhook.subsystems.SUBSYS_LABEL_COLOR,
                          'description': webhook.subsystems.SUBSYS_LABEL_DESC % 'net'},
                         {'name': 'lnst::NeedsTesting',
                          'color': webhook.common.NEEDS_TESTING_LABEL_COLOR,
                          'description': webhook.subsystems.NEEDS_TESTING_LABEL_DESC % 'lnst'},
                         ]

        result = webhook.subsystems.make_labels(topics)
        self.assertEqual(len(result), 4)
        self.assertEqual(result, expected_list)

    def test_get_current_subsystems_from_labels(self):
        mock_mr = mock.Mock()
        mock_mr.labels = ['Subsystem:net', 'Subsystem:ethernet', 'Subsystem:wireless']
        output = webhook.subsystems.get_current_subsystems_from_labels(mock_mr)
        self.assertEqual(output, ['net', 'ethernet', 'wireless'])

    def test_get_stale_subsystems(self):
        old = ['net', 'ethernet', 'wireless']
        new = ['net', 'ethernet']
        output = webhook.subsystems.get_stale_subsystems(old, new)
        self.assertEqual(output, ['wireless'])

    @mock.patch('webhook.common.remove_labels_from_merge_request')
    def test_remove_stale_subsystem_labels(self, mock_remove):
        mock_proj = mock.Mock()
        mr_id = '100'
        stale = ['wireless']
        webhook.subsystems.remove_stale_subsystem_labels(mock_proj, mr_id, stale)
        mock_remove.assert_called_with(mock_proj, mr_id, ['Subsystem:wireless'])

    @mock.patch('webhook.cdlib.get_filtered_changed_files')
    @mock.patch('webhook.cdlib.set_dependencies_label')
    def test_get_mr_pathlist(self, dep_label, files):
        mock_proj = mock.Mock()
        mock_mr = mock.Mock()
        mock_mr.changes.return_value = {}
        dep_label.return_value = "Dependencies::OK"
        files.return_value = ['include/linux/netdevice.h']
        result = webhook.subsystems.get_mr_pathlist(mock_proj, mock_mr)
        self.assertEqual(result, [])
        mock_mr.changes.return_value = self.CHANGES
        result = webhook.subsystems.get_mr_pathlist(mock_proj, mock_mr)
        self.assertEqual(sorted(result), ['drivers/net/ethernet/intel/ixgbe/ixgbe.h',
                                          'include/linux/mm.h',
                                          'include/linux/mm2.h',
                                          'include/net/bond_xor.h',
                                          'include/net/bonding.h'])
        dep_label.return_value = "Dependencies::abcdefg"
        result = webhook.subsystems.get_mr_pathlist(mock_proj, mock_mr)
        self.assertEqual(result, ['include/linux/netdevice.h'])

    @mock.patch('webhook.common.remove_labels_from_merge_request')
    @mock.patch('webhook.common.Message.gl_instance')
    @mock.patch('webhook.subsystems.post_notifications')
    @mock.patch('webhook.subsystems.do_usermapping')
    @mock.patch('webhook.common.add_label_to_merge_request')
    @mock.patch('webhook.subsystems.get_mr_pathlist')
    def test_process_mr(self, mock_pathlist, mock_add_label, mock_usermapping,
                        mock_poster, mock_gl, mock_remove):
        project_url = 'https://gitlab.com/project123'
        mock_mr = mock.Mock()
        mock_mr.iid = 2
        mock_mr.commits.return_value = [1]
        mock_mr.target_branch = 'main'
        mock_project = mock.Mock()
        mock_project.namespace = {'name': '8.y'}
        mock_gl.projects.get.return_value = mock_project
        mock_project.mergerequests.get.return_value = mock_mr
        mock_mr.labels = []
        mock_pathlist.return_value = []
        mock_usermapping.return_value = []

        msg = mock.Mock()
        msg.payload = {}
        msg.payload['project'] = {'id': 1}
        msg.payload['changes'] = {'labels': {'previous': [],
                                             'current': [{'title': 'Subsystem:'}]
                                             }
                                  }
        msg.payload['object_attributes'] = {'iid': 2, 'action': 'open'}

        # No path changes.
        with self.assertLogs('cki.webhook.subsystems', level='INFO') as logs:
            owners_parser = webhook.owners.Parser(None)
            webhook.subsystems.process_mr(mock_gl, msg, owners_parser, '/data/repo', project_url)
            self.assertIn('MR 2 does not report any changed files, exiting.', logs.output[-1])
            mock_usermapping.assert_not_called()
            mock_poster.assert_not_called()
            mock_add_label.assert_not_called()

        owners_parser = webhook.owners.Parser(self.OWNERS_YAML)

        # No label to add.
        with self.assertLogs('cki.webhook.subsystems', level='INFO') as logs:
            mock_pathlist.return_value = ['weirdfile.bat']
            webhook.subsystems.process_mr(mock_gl, msg, owners_parser, '/data/repo', project_url)
            self.assertIn('No labels to add.', ' '.join(logs.output))
            mock_add_label.assert_not_called()
            mock_usermapping.assert_called()

        # Label, no users to map.
        with self.assertLogs('cki.webhook.subsystems', level='INFO') as logs:
            mock_pathlist.return_value = ['net/core/dev.c']
            name = f'{webhook.subsystems.SUBSYS_LABEL_PREFIX}:net'
            color = webhook.subsystems.SUBSYS_LABEL_COLOR
            desc = webhook.subsystems.SUBSYS_LABEL_DESC % 'net'
            label = {'name': name, 'color': color, 'description': desc}
            tname = f'lnst::{webhook.common.NEEDS_TESTING_SUFFIX}'
            tcolor = webhook.common.NEEDS_TESTING_LABEL_COLOR
            tdesc = webhook.subsystems.NEEDS_TESTING_LABEL_DESC % 'lnst'
            tlabel = {'name': tname, 'color': tcolor, 'description': tdesc}
            mock_usermapping.return_value = []
            webhook.subsystems.process_mr(mock_gl, msg, owners_parser, '/data/repo', project_url)
            call_list = [mock.call(mock_project, 2, [label]),
                         mock.call(mock_project, 2, [tlabel])]
            self.assertEqual(mock_add_label.call_count, 2)
            mock_add_label.assert_has_calls(call_list, any_order=True)
            mock_usermapping.assert_called_with('8.y', mock_pathlist.return_value, '/data/repo')
            mock_poster.assert_not_called()
            self.assertIn('No new users to notify @ for MR 2.', logs.output[-1])

        # Labels & user mapped.
        mock_usermapping.return_value = ['user1']
        webhook.subsystems.process_mr(mock_gl, msg, owners_parser, '/data/repo', project_url)
        self.assertEqual(mock_add_label.call_count, 4)
        mock_add_label.assert_has_calls(call_list, any_order=True)
        mock_usermapping.assert_called_with('8.y', mock_pathlist.return_value, '/data/repo')
        mock_poster.assert_called_with(mock_mr, ['user1'], project_url)

        # Stale labels to remove.
        mock_mr.labels = ['Subsystem:net', 'Subsystem:ethernet']
        mock_pathlist.return_value = ['net/core/dev.c']
        webhook.subsystems.process_mr(mock_gl, msg, owners_parser, '/data/repo', project_url)
        mock_remove.assert_called_with(mock_project, mock_mr.iid, ['Subsystem:ethernet'])
