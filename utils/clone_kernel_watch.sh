#!/usr/bin/env bash

# This script clones or updates the kernel-watch-repo used by
# the subsystems webhook.

set -e

LOCAL_REPO_PATH=${LOCAL_REPO_PATH:-/data/subsystems/kernel-watch}
KERNEL_WATCH_URL=${KERNEL_WATCH_URL:-https://gitlab.com/redhat/rhel/src/kernel/watch.git}

echo "LOCAL_REPO_PATH: ${LOCAL_REPO_PATH}"
echo "KERNEL_WATCH_URL: ${KERNEL_WATCH_URL}"

GIT_URL=${KERNEL_WATCH_URL}
GIT_URL_LC="${GIT_URL,,}"
GIT_PROTO="${GIT_URL_LC%%://*}"
GIT_HOST="${GIT_URL_LC#*//}"
GIT_HOST="${GIT_HOST%%/*}"
GIT_REPO_PATH="${GIT_URL_LC#*//*/}"

TOKEN="${COM_GITLAB_RO_REPO_TOKEN}"
if [[ -z ${TOKEN} ]]; then
	echo "No Token found."
	exit 1
fi
GIT_URL="${GIT_PROTO}://oauth2:${TOKEN}@${GIT_HOST}/${GIT_REPO_PATH}"

if [[ -d ${LOCAL_REPO_PATH} ]]; then
	cd ${LOCAL_REPO_PATH}
	git pull
else
	mkdir -p ${LOCAL_REPO_PATH}
	git clone ${GIT_URL} ${LOCAL_REPO_PATH}
fi
