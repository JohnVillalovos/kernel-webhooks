#!/usr/bin/env bash

# This script sets up and maintains a Red Hat kernel git repo,
# with all merge request references fetched for examination

set -e

. cki_utils.sh

ABSPATH=${BASH_SOURCE[0]}
BASEDIR=$(dirname ${ABSPATH})
RHKERNEL_SRC=${RHKERNEL_SRC:-/usr/src/rhkernel}

# We start with kernel-ark, which is public and constantly updating
rhkernel="https://gitlab.com/cki-project/kernel-ark.git"
ark_branch="os-build"
repolist="$(pwd)/${BASEDIR}/rh_kernel_git_repos.txt"

if [ ! -d ${RHKERNEL_SRC} ]; then
    mkdir -p ${RHKERNEL_SRC}
fi

GIT_URL=${rhkernel}
GIT_URL_LC="${GIT_URL,,}"
GIT_PROTO="${GIT_URL_LC%%://*}"
GIT_HOST="${GIT_URL_LC#*//}"
GIT_HOST="${GIT_HOST%%/*}"
GIT_REPO_PATH="${GIT_URL_LC#*//*/}"

TOKEN="${COM_GITLAB_RO_REPO_TOKEN}"
if [ -z "${TOKEN}" ]; then
    echo "No gitlab token found, bailing early, assume we're in container build phase w/o creds yet"
    exit
fi

GIT_URL="${GIT_PROTO}://oauth2:${TOKEN}@${GIT_HOST}/${GIT_REPO_PATH}"

if [ ! -d ${RHKERNEL_SRC}/.git ]; then
    echo "Cloning kernel git tree into ${RHKERNEL_SRC}"
    git clone ${GIT_URL} ${RHKERNEL_SRC}
    if [ $? -ne 0 ]; then
      echo "Something went wrong trying to clone ${GIT_HOST}/${GIT_REPO_PATH}"
      exit 1
    fi
    cd ${RHKERNEL_SRC}
    git config pull.ff only
else
    echo "Updating existing kernel git tree at ${RHKERNEL_SRC}"
    cd ${RHKERNEL_SRC}
    git config pull.ff only
    if [ $(git branch | grep -c ${ark_branch}) -eq 0 ]; then
      git checkout -b ${ark_branch} -t origin/${ark_branch}
    elif [ $(git branch --show-current) != ${ark_branch} ]; then
      git checkout ${ark_branch}
    fi
    git pull
    if [ $? -ne 0 ]; then
      echo "Something went wrong trying to pull from ${GIT_HOST}/${GIT_REPO_PATH}"
      exit 1
    fi
fi


# This adds all the RHEL kernel projects and fetches their MR refs
while IFS= read -r line
do
    # Skip comment lines. File format documented in ${repolist}.
    if [ $(echo ${line} | grep -c "^#") -eq 1 ]; then
        continue
    fi
    name=$(echo ${line} | cut -f1 -d=)
    GIT_URL=$(echo ${line} | cut -f2 -d=)
    GIT_URL_LC="${GIT_URL,,}"
    GIT_PROTO="${GIT_URL_LC%%://*}"
    GIT_HOST="${GIT_URL_LC#*//}"
    GIT_HOST="${GIT_HOST%%/*}"
    GIT_REPO_PATH="${GIT_URL_LC#*//*/}"
    GIT_URL="${GIT_PROTO}://oauth2:${TOKEN}@${GIT_HOST}/${GIT_REPO_PATH}"

    if [ $(git remote | grep -c ${name}) -eq 0 ]; then
        echo "Adding git remote ${name}"
        git remote add ${name} ${GIT_URL}
	git config remote.${name}.fetch "+refs/merge-requests/*/head:refs/remotes/${name}/merge-requests/*"
    fi
    if [ $(git remote | grep -c ${name}-branches) -eq 0 ]; then
        echo "Adding git remote ${name}-branches"
        git remote add ${name}-branches ${GIT_URL}
    fi
    echo "Fetching remote MR refs for ${name}"
    git fetch ${name}-branches
    if [ $? -ne 0 ]; then
      echo "Something went wrong trying to fetch ${name}-branches"
      exit 1
    fi
    git fetch ${name}
    if [ $? -ne 0 ]; then
      echo "Something went wrong trying to fetch ${name}"
      exit 1
    fi
done < "${repolist}"

echo "All done."
