"""Update bugs to contain links to an MR and successful pipeline artifacts."""
from configparser import ConfigParser
import sys

from bugzilla import BugzillaError
from cki_lib import logger
from cki_lib import misc
from cki_lib import session

from . import common

LOGGER = logger.get_logger('cki.webhook.buglinker')
SESSION = session.get_session('cki.webhook.buglinker')

KERNEL_BZ_BOT = 'cki-ci-bot+kernel-workflow-bugzilla@redhat.com'
KERNEL_PIPELINE = 'trigger_pipeline'


def update_bugzilla(project, bugs, mr_id, bzcon):
    """Filter input bug list and run bugzilla API actions."""
    if not bugs:
        LOGGER.debug('Input bug list is empty.')
        return
    # Ignore non-numeric bugs such as 'INTERNAL'. Bug list items are strings :/.
    bugs_to_exclude = list(filter(lambda bug: not bug.isdigit(), bugs))
    if bugs_to_exclude:
        LOGGER.info('Excluding these "bugs" from bugzilla actions: %s', bugs_to_exclude)
        if bugs_to_exclude == bugs:
            return
    bugs_filtered = [item for item in bugs if item not in bugs_to_exclude]
    add_mr_to_bz(bzcon, project, bugs_filtered, mr_id)


def bz_is_linked_to_mr(bug, domain, path):
    """Return True if any of the bug's trackers point to the given MR."""
    if not bug.external_bugs:
        return False
    for tracker in bug.external_bugs:
        if tracker['type']['description'] != "Gitlab":
            continue
        if tracker['type']['url'].split('/')[2] == domain and \
           tracker['ext_bz_bug_id'] == path:
            return True
    return False


def add_mr_to_bz(bzcon, project, bug_list, mr_id):
    """Take a list of bugs and a MR# and update the BZ's ET with the MR url."""
    if not bug_list:
        LOGGER.error("MR %d has no bugs? Skipping linking of bugs to MR.", mr_id)
        return

    bz_results = bzcon.getbugs(bug_list)
    if not bz_results:
        LOGGER.info("getbugs() returned an empty list for these bugs: %s.", bug_list)
        return

    # Properties that we want to match when checking a BZ's trackers
    mr_domain = project.web_url.split('/')[2]
    mr_path = f"{project.path_with_namespace}/-/merge_requests/{mr_id}"

    untracked_bugs = list(filter(lambda bug: not bz_is_linked_to_mr(bug, mr_domain, mr_path),
                                 bz_results))
    if not untracked_bugs:
        LOGGER.info("All bugs have an existing link to MR %d.", mr_id)
        return

    new_bugs = list(map(lambda bug: bug.id, untracked_bugs))
    LOGGER.info("Need to add MR %d to external tracker list of these bugs: %s", mr_id, new_bugs)
    if misc.is_production():
        ext_domain = f"https://{mr_domain}/"
        try:
            bzcon.add_external_tracker(new_bugs, ext_type_url=ext_domain, ext_bz_bug_id=mr_path)
        except BugzillaError:
            LOGGER.exception("Problem adding tracker %s%s to BZs.", ext_domain, mr_path)


def post_to_bugs(bug_list, post_text, pipeline_url, bzcon):
    """Submit post to given bugs."""
    filtered_bugs = []
    try:
        # It is possible for there to be multiple 'success' events for a given pipeline
        # so we should check that we haven't already posted them to the bugs. Sigh.
        bugs = bzcon.getbugs(bug_list)
        for bug in bugs:
            if not comment_already_posted(bug, pipeline_url):
                filtered_bugs.append(bug.id)
        if not filtered_bugs:
            LOGGER.info('Pipeline results have already been posted to all relevant bugs.')
            return

        if misc.is_production():
            comment = bzcon.build_update(comment=post_text)
            bzcon.update_bugs(filtered_bugs, comment)
    except BugzillaError:
        LOGGER.exception('Error posting comments to bugs: %s', filtered_bugs)
        return
    LOGGER.info('Posted comment to bugs: %s', filtered_bugs)


def comment_already_posted(bug, pipeline_url):
    """Return True if the pipeline results have already been posted to the given bug."""
    pipeline_text = f'Pipeline: {pipeline_url}'
    comments = bug.getcomments()
    for comment in comments:
        if comment['creator'] == KERNEL_BZ_BOT and pipeline_text in comment['text']:
            LOGGER.info('Excluding bug %s as pipeline was already posted in comment %s.', bug.id,
                        comment['count'])
            return True
    return False


def create_bugzilla_text(title, mr_url, pipeline_url, job_data):
    """Return formatted job data."""
    bz_post = 'The following Merge Request has pipeline job artifacts available:\n\n'
    bz_post += f'Title: {title}\nMR: {mr_url}\n'
    bz_post += f'Pipeline: {pipeline_url}\n\n'
    bz_post += ('This Repo URL is *not* accessible from a web browser!'
                ' It only functions as a dnf or yum baseurl.\n')

    repo_url = job_data[0]['repo_url'].replace(job_data[0]['arch'], '$basearch')
    bz_post += f'Repo URL: {repo_url}\n\n'

    for job in job_data:
        bz_post += '\n'
        bz_post += f"{job['version']}.{job['arch']}:\n"
        bz_post += f"Job: {job['job_url']}\n"
        bz_post += f"Gitlab browser: {job['browse_url']}\n"
        bz_post += f"Current automated test status: {job['test_job_status']}\n"
    return bz_post


def parse_job_rc(job, test_job_status):
    """Return a dict with the arch, kernel version, and URLs from a job rc file."""
    rc_file = job.artifact('rc')
    if not rc_file:
        return {}

    rc_config = ConfigParser()
    rc_config.read_string(rc_file.decode())

    browse_url = f"{job.web_url}/artifacts/browse/{rc_config['state']['repo_path']}"

    job_dict = {'id': job.id,
                'arch': rc_config['state']['kernel_arch'],
                'version': rc_config['state']['kernel_version'],
                'test_job_status': test_job_status,
                'browse_url': browse_url,
                'job_url': job.web_url,
                'repo_url': rc_config['state']['kernel_package_url']
                }
    return job_dict


def get_pipeline_job_ids(pipeline):
    """Return a dict of the successful publish job IDs with the status of their test job."""
    jobs_dict = {}
    jobs = pipeline.jobs.list()
    for job in jobs:
        if job.stage == 'publish' and job.status == 'success':
            arch = job.name.split()[-1]
            jobs_dict[job.id] = next((job.status for job in jobs if job.name == f'test {arch}'),
                                     'Unknown')
    return jobs_dict


def get_downstream_project_pipeline(gl_instance, project, local_pipeline_id, bridge_name):
    """Return the downstream project & pipeline object from the matching local pipeline bridge."""
    local_pipeline = project.pipelines.get(local_pipeline_id)
    bridge = next((brdg for brdg in local_pipeline.bridges.list() if brdg.name == bridge_name),
                  None)
    if bridge is None or bridge.downstream_pipeline is None:
        return (None, None)

    downstream_pipeline_id = bridge.downstream_pipeline.get('id')
    downstream_path = parse_gl_project_path(bridge.downstream_pipeline['web_url'])
    downstream_project = gl_instance.projects.get(downstream_path)
    downstream_pipeline = downstream_project.pipelines.get(downstream_pipeline_id)
    return (downstream_project, downstream_pipeline)


def parse_gl_project_path(url):
    """Return the project path with namespace from the given Gitlab url."""
    url_split = url.split('/')[3:]
    namespace = []
    for part in url_split:
        if part == '-':
            break
        namespace.append(part)
    return '/'.join(namespace)


def bugs_in_mr_description(description):
    """Return the list of all the Bugzilla: bugs mentioned in an MR description."""
    if not description:
        return []
    bugs = set([])
    for line in description.splitlines():
        bug = common.find_bz_in_line(line, 'Bugzilla')
        if bug:
            bugs.add(bug)
    return list(bugs)


def mr_event_should_run_pipeline(msg):
    """Is true if MR has readyForQA & event shows WIP status removed or readyForQA label added."""
    if 'title' in msg.payload['changes']:
        old_title = msg.payload['changes']['title'].get('previous', '')
        new_title = msg.payload['changes']['title'].get('current', '')

        str_tuple = ('[Draft]', 'Draft:', '(Draft)', '[WIP]', 'WIP:')
        if old_title and (old_title.startswith(str_tuple) and not new_title.startswith(str_tuple)):
            return True
    return False


def process_pipeline(gl_instance, msg):
    """Parse pipeline or merge_request msg and run _process_pipeline."""
    if msg.payload['object_kind'] == 'pipeline':
        mr_id = msg.payload["merge_request"]["iid"]
    elif msg.payload['object_kind'] == 'merge_request':
        mr_id = msg.payload["object_attributes"]["iid"]

    project = gl_instance.projects.get(msg.payload["project"]["id"])
    merge_request = project.mergerequests.get(mr_id)
    if merge_request.work_in_progress:
        LOGGER.info("MR %s is marked work in progress, ignoring.", mr_id)
        return
    if merge_request.head_pipeline is None:
        LOGGER.info("MR %s has not triggered any pipelines? head_pipeline is None.", mr_id)
        return

    pipeline_id = merge_request.head_pipeline.get('id')
    if not merge_request.head_pipeline.get('web_url', '').startswith('https://gitlab.com/redhat/'):
        LOGGER.info("MR %s head pipeline #%s is not in the Red Hat namespace: %s", mr_id,
                    pipeline_id, merge_request.head_pipeline.get('web_url'))
        return

    _process_pipeline(gl_instance, project, pipeline_id, merge_request)


def _process_pipeline(gl_instance, project, pipeline_id, merge_request):
    # pylint: disable=too-many-locals
    """Process a successful pipeline event message."""
    # If the MR doesn't have any bugs listed, then we have nothing to do.
    bug_list = bugs_in_mr_description(merge_request.description)
    if not bug_list:
        LOGGER.info('No bugs found in MR %s description.', merge_request.iid)
        return

    # Get downstream objects.
    (downstream_project, downstream_pipeline) = get_downstream_project_pipeline(gl_instance,
                                                                                project,
                                                                                pipeline_id,
                                                                                KERNEL_PIPELINE)

    if downstream_project is None or downstream_pipeline is None:
        LOGGER.info('No downstream pipeline found for local pipeline %s.', pipeline_id)
        return

    # If the pipeline isn't finished give up and wait for next time.
    if downstream_pipeline.status not in ('success', 'failed'):
        LOGGER.info('Pipeline %s is not finished: %s', downstream_pipeline.id,
                    downstream_pipeline.status)
        return

    # If the publish jobs were not successful then we have nothing to do.
    jobs_dict = get_pipeline_job_ids(downstream_pipeline)
    if not jobs_dict:
        LOGGER.info('No successful publish jobs found for downstream pipeline %s in project %s.',
                    downstream_pipeline.id, downstream_project.name)
        return

    # Process each job and for each one store necessary facts in a dict.
    jobs_data = []
    for job_id, status in jobs_dict.items():
        # "Job methods (play, cancel, and so on) are not available on ProjectPipelineJob objects.
        # To use these methods create a ProjectJob object."
        job_data = parse_job_rc(downstream_project.jobs.get(job_id), status)
        if job_data:
            jobs_data.append(job_data)
    if not jobs_data:
        LOGGER.debug('No job data.')
        return

    # Connect to bugzilla and post the comment.
    bzcon = common.try_bugzilla_conn()
    if not bzcon:
        LOGGER.error('No bugzilla connection.')
        return

    pipeline_url = f'https://gitlab.com/{project.path_with_namespace}/-/pipelines/{pipeline_id}'
    bug_post_text = create_bugzilla_text(merge_request.title, merge_request.web_url,
                                         pipeline_url, jobs_data)
    LOGGER.info('Creating bug comment on bugs: %s\n%s', sorted(bug_list), bug_post_text)
    post_to_bugs(bug_list, bug_post_text, pipeline_url, bzcon)


def process_mr(gl_instance, msg):
    """Process a merge request event message."""
    # Has the WIP flag been removed?
    # Then run the pipeline linker.
    if mr_event_should_run_pipeline(msg):
        LOGGER.info('Sending MR event to pipeline processor.')
        process_pipeline(gl_instance, msg)
        return

    # If the MR file contents haven't changed then don't run.
    commits_changed = common.mr_action_affects_commits(msg)
    if not commits_changed:
        return

    bzcon = common.try_bugzilla_conn()
    if not bzcon:
        return

    project = gl_instance.projects.get(msg.payload["project"]["id"])
    merge_request = project.mergerequests.get(msg.payload["object_attributes"]["iid"])

    bugs = bugs_in_mr_description(merge_request.description)
    if not bugs:
        LOGGER.info('No bugs found in MR %s description.', merge_request.iid)
        return

    update_bugzilla(project, bugs, merge_request.iid, bzcon)


WEBHOOKS = {
    "merge_request": process_mr,
    'pipeline': process_pipeline,
}


def main(args):
    """Run main loop."""
    parser = common.get_arg_parser('BUGLINKER')
    args = parser.parse_args(args)
    common.generic_loop(args, WEBHOOKS, ignore_msgs_from_self=args.dont_ignore_self)


if __name__ == "__main__":
    main(sys.argv[1:])
