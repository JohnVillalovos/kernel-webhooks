"""Query bugzilla for MRs."""
import enum
import json
import re
import sys

from cki_lib import logger
from cki_lib import misc
from cki_lib import session
from gitlab.exceptions import GitlabGetError

from . import cdlib
from . import common

LOGGER = logger.get_logger('cki.webhook.bugzilla')
SESSION = session.get_session('cki.webhook.bugzilla')

BZ_READY_FOR_MERGE_LABEL = "This merge request's bugzillas are approved for integration."
BZ_READY_FOR_QA_LABEL = "This merge request's bugzillas are ready for QA testing."
BZ_NEEDS_REVIEW_LABEL = ('This merge request has bugzillas associated with it that are not yet'
                         ' approved for integration, or commits without a valid bug'
                         ' referenced in them.')


def get_project_file(project, branch, path, raw=True):
    """Return the given file from the project."""
    try:
        if raw:
            return project.files.raw(file_path=path, ref=branch)
        return project.files.get(file_path=path, ref=branch)
    except GitlabGetError:
        LOGGER.info('Failed to fetch. project: %s, branch: %s, path: %s, raw enabled: %s.',
                    project.name, branch, path, raw)
    return None


def get_project_major_ver(project):
    """Return the major version of the given project."""
    # All current rhel src kernel projects are named 'rhel-<major>'.
    major_ver = re.match(r'^rhel-(alt-)?(?P<version>\d+)', project.name)
    if major_ver:
        return major_ver['version']
    LOGGER.debug("Unable to determine major version of project '%s' from its name.", project.name)

    # Try to get RHEL_MAJOR from the project's 'main' repo Makefiles. Barf.
    # In older releases Makefile.rhelver doesn't exist.
    makefile = get_project_file(project, 'main', 'Makefile.rhelver')
    if not makefile:
        makefile = get_project_file(project, 'main', 'Makefile')

    if not makefile:
        return None

    for line in makefile.splitlines():
        if line.decode().startswith('RHEL_MAJOR = '):
            return line.decode().split(' = ')[1].strip()

    LOGGER.error('Cannot find RHEL_MAJOR in Makefile.')
    return None


def generate_refname(major, minor, alt, suffix=''):
    """Generate the refname for gitbz."""
    if int(major) < 8:
        return f"rhel{alt}{major}.{minor}{suffix}"
    return f"rhel-{major}.{minor}.0{suffix}"


def get_mr_target_release(project, mr_target):
    """Determine the target release for the given MR."""
    major_ver = get_project_major_ver(project)
    if not major_ver:
        return None

    alt = '-alt-' if '-alt-' in project.name else '-'

    # If the target branch of the MR is not 'main' then the MR is pointing
    # at a z-stream branch or an early RHEL major release (Alpha or Beta)
    if mr_target != 'main':
        target_re = fr"^{major_ver}\.(\d+)(-([a-z]+)?)?$"
        regex = re.compile(target_re)
        match = regex.fullmatch(mr_target)
        if not match:
            return None
        minor_ver = match.group(1)
        suffix = "" if match.group(3) != "alpha" or minor_ver != "0" else "-alpha"
        suffix = suffix if match.group(3) != "beta" or minor_ver != "0" else "-beta"
        return generate_refname(major_ver, minor_ver, alt, suffix)

    # If the target branch is 'main' then we're aiming at the latest release.
    branch_search = f"^{major_ver}."
    branches = project.branches.list(search=branch_search)
    minor_ver = -1
    branch_regex = fr"^{major_ver}\.(\d+)$"
    regex = re.compile(branch_regex)
    for branch in branches:
        match = regex.fullmatch(branch.name)
        if match:
            branch_minor = int(match.group(1))
            if minor_ver < branch_minor:
                minor_ver = branch_minor
    minor_ver += 1
    return generate_refname(major_ver, minor_ver, alt)


def nobug_msg():
    """Return the message to use as a note for commits without bz."""
    msg = "No 'Bugzilla:' entry was found in these commits.  This project requires that each "
    msg += "commit have at least one 'Bugzilla:' entry.  Please add a 'Bugzilla: <bugzilla_URL>' "
    msg += "entry for each bugzilla.  Guidelines for 'Bugzilla:' entries can be found in "
    msg += "CommitRules, https://red.ht/kwf_commit_rules."
    return msg


def depbug_msg():
    """Return the message to use as note for commits that are dependencies."""
    msg = "These commits are for dependencies listed in the MR description.  Your MR will not be "
    msg += "merged until these dependencies are in main."
    return msg


def nomrbug_msg():
    """Return the message to note an unknown bug found in patches."""
    msg = "These commits contain a bugzilla number that was not listed in the merge request "
    msg += "description.  Please verify the bugzilla's URL is correct, or, add them to your MR "
    msg += "description as either a 'Bugzilla:' or a 'Depends:' entry.  Guidelines for these "
    msg += "entries can be found in CommitRules, https://red.ht/kwf_commit_rules."
    return msg


def notapproved_msg():
    """Return the message for bugs which are not approved."""
    msg = "The bugzilla associated with these commits is not approved at this time."
    return msg


def badinternalbug_msg():
    """Return the message that 'INTERNAL' bug touches wrong source files."""
    msg = "These commits are marked as 'INTERNAL' but were found to touch "
    msg += "source files outside of the redhat/ directory. 'INTERNAL' bugs "
    msg += "are only to be used for changes to files in the redhat/ directory."
    return msg


def validate_internal_bz(commit_list):
    """Check file list of 'INTERNAL' bug only touch files under redhat/ or .gitlab*."""
    allowed_paths = ['redhat/', '.gitlab']
    for commit in commit_list.values():
        for path in commit:
            if not path.startswith(tuple(allowed_paths)):
                return False
    return True


BUG_FIELDS = ['cf_verified',
              'component',
              'id',
              'keywords',
              'product',
              'status'
              ]


def bug_is_verified(bug_no, bzcon):
    """Confirm the bug is Verified."""
    bug = bzcon.getbug(bug_no, include_fields=BUG_FIELDS)
    if not bug or bug.id != int(bug_no):
        LOGGER.error("getbug() did not return a useful result for BZ %s.", bug_no)
        return False

    if bug.product.startswith('Red Hat Enterprise Linux') and bug.component == 'kernel' \
            and 'Tested' in bug.cf_verified:
        LOGGER.debug('Bug %s is Verified.', bug.id)
        return True
    LOGGER.debug('Bug %s is not Verified.', bug.id)
    return False


class BZState(enum.IntEnum):
    """Relevant BZ states for an MR."""

    NOT_READY = 0
    READY_FOR_QA = 1
    READY_FOR_MERGE = 2


def validate_bzs(project, merge_request, bugzilla_ids, bz_dependencies, reviewed_items, bzcon):
    # pylint: disable=too-many-locals,too-many-branches,too-many-statements,too-many-arguments
    """Validate bugzilla references."""
    target_branch = get_mr_target_release(project, merge_request.target_branch)
    alt = '-alt' if '-alt-' in project.name else ''
    notes = []
    nobug_id = ""
    depbug_id = ""
    nomrbug_id = ""
    notgt_id = ""
    internalbug_id = ""
    for bug in reviewed_items:
        bz_properties = {'approved': BZState.NOT_READY, 'notes': [], 'logs': ''}
        reviewed_items[bug]["validation"] = bz_properties
        valid_mr_bug = True

        if not target_branch:
            notes += [] if notgt_id else ["Unknown repository target"]
            notgt_id = notgt_id if notgt_id else str(len(notes))
            bz_properties['notes'].append(notgt_id)
            continue

        if bug == 'INTERNAL':
            if validate_internal_bz(reviewed_items[bug]['commits']):
                bz_properties['approved'] = BZState.READY_FOR_MERGE
            else:
                notes += [] if internalbug_id else [badinternalbug_msg()]
                internalbug_id = internalbug_id if internalbug_id else str(len(notes))
                bz_properties['notes'].append(internalbug_id)
            continue

        if bug == "-":
            notes += [] if nobug_id else [nobug_msg()]
            nobug_id = nobug_id if nobug_id else str(len(notes))
            bz_properties['notes'].append(nobug_id)
            continue

        if bug in bz_dependencies:
            notes += [] if depbug_id else [depbug_msg()]
            depbug_id = depbug_id if depbug_id else str(len(notes))
            bz_properties['notes'].append(depbug_id)

        elif bug not in bugzilla_ids:
            notes += [] if nomrbug_id else [nomrbug_msg()]
            nomrbug_id = nomrbug_id if nomrbug_id else str(len(notes))
            bz_properties['notes'].append(nomrbug_id)
            valid_mr_bug = False

        buginputs = {
            'package': f'kernel{alt}',
            'namespace': 'rpms',
            'ref': 'refs/heads/' + target_branch,
            'commits': [{
                'hexsha': 'HEAD',
                'files': ["kernel.spec"],
                'resolved': [int(bug)],
                'related': [],
                'reverted': [],
            }]
        }
        print(json.dumps(buginputs, indent=2))
        # NEED TO FIND A BETTER WAY TO DO THIS - FIXME!
        bugresults = SESSION.post(
            "https://dist-git.host.prod.eng.bos.redhat.com"
            "/lookaside/gitbz-query.cgi",
            json=buginputs)

        resjson = bugresults.json()
        print(json.dumps(resjson, indent=2))
        if valid_mr_bug and bzcon and resjson['result'] == 'ok':
            if bug_is_verified(bug, bzcon):
                bz_properties['approved'] = BZState.READY_FOR_MERGE
            else:
                bz_properties['approved'] = BZState.READY_FOR_QA
        else:
            bz_properties['approved'] = BZState.NOT_READY
            noteid = 0
            while noteid < len(notes):
                if notes[noteid] == notapproved_msg():
                    break
                noteid += 1
            if noteid == len(notes):
                notes.append(notapproved_msg())
            bz_properties['notes'].append(str(noteid+1))
    return (notes, target_branch)


def get_report_table(reviewed_items):
    """Create the table for the bugzilla report."""
    mr_approved = True
    table = []
    for bug in reviewed_items:
        bz_properties = reviewed_items[bug]["validation"]
        mr_approved = mr_approved and bz_properties['approved']
        commits = []
        for commit in reviewed_items[bug]["commits"]:
            commits.append(commit)
        table.append([bug, commits, bz_properties['approved'].name,
                      bz_properties['notes'], bz_properties['logs']])
    return (table, mr_approved)


def trim_notes(notes):
    """Trim/remove unwanted notes returned from dist-git cgi script."""
    noteid = 0
    while noteid < len(notes):
        trimmed = ""
        if notes[noteid] is None:
            noteid += 1
            continue
        strings = notes[noteid].split('\n')
        for string in strings:
            drop = False
            drop = string.startswith("*** eg: Resolves: rhbz#") or drop
            drop = string.startswith("*** Make sure to follow the") or drop
            drop = string.startswith("*** No issue IDs referenced") or drop
            drop = string.startswith("*** No approved issue") or drop
            drop = string.startswith("*** Unapproved issue") or drop
            drop = string.startswith("*** Commit") or drop
            trimmed += "" if drop else string + '\n'
        notes[noteid] = trimmed
        noteid += 1


def print_target(merge_request, bz_target):
    """Print the bugzilla target if MR targets main branch."""
    target = merge_request.target_branch
    target = target if target != "main" else f"main ({bz_target})"
    return "Target Branch: " + target


def print_gitlab_report(merge_request, notes, bz_target, table, mr_approved):
    """Print a bugzilla report for gitlab."""
    report = "<br>\n\n**Bugzilla Readiness "
    if mr_approved:
        report += "Report**\n\n"
    else:
        report += "Error(s)!**\n\n"
    report += print_target(merge_request, bz_target) + "   \n"
    report += " \n\n"
    report += "|BZ|Commits|Approved|Notes|\n"
    report += "|:------|:------|:------|:-------|\n"
    for row in table:
        bzn = f"BZ-{row[0]}" if row[0] != "-" else row[0]
        commits = row[1] if len(row[1]) < 26 else row[1][:25] + ["(...)"]
        report += "|"+bzn+"|"+"<br>".join(commits)
        report += "|"+str(row[2])+"|"
        report += common.build_note_string(row[3])

    report += "\n\n"
    report += common.print_notes(notes)
    if mr_approved:
        report += "Merge Request passes bugzilla validation\n"
    else:
        report += "\nMerge Request fails bugzilla validation.  \n"
        report += " \n"
        report += "To request re-evalution after getting bugzilla approval "
        report += "add a comment to this MR with only the text: "
        report += "request-bz-evaluation "

    return report


def print_text_report(merge_request, notes, bz_target, table, mr_approved):
    """Print a bugzilla report for the text console."""
    report = "\n\nBZ Readiness Report\n\n"
    report += print_target(merge_request, bz_target) + "\n\n"
    for row in table:
        commits = common.build_commits_for_row(row)
        report += "|"+str(row[0])+"|"+" ".join(commits)
        report += "|"+str(row[2])+"|"+", ".join(row[3])+"|\n\n"
    report += common.print_notes(notes)
    status = "passes" if mr_approved else "fails"
    report += "Merge Request %s bugzilla validation\n" % status
    return report


def build_review_lists(commit, review_lists, bug):
    """Build review_lists for this bug."""
    found_files = []
    if bug == 'INTERNAL':
        found_files = cdlib.extract_files(commit)

    bugd = review_lists[bug] if bug in review_lists else {}
    commits = bugd["commits"] if "commits" in bugd else {}
    commits[commit.id] = found_files
    bugd["commits"] = commits
    return bugd


def check_on_bzs(project, merge_request, bugzilla_ids, bz_dependencies, bzcon):
    # pylint: disable=too-many-locals
    """Check BZs."""
    review_lists = {}
    for commit in merge_request.commits():
        commit = project.commits.get(commit.id)
        # Skip merge commits, bugzilla link on the commit log for them
        # is not required and we do not care if bugs are listed on them
        if len(commit.parent_ids) > 1:
            continue
        try:
            found_bzs, non_mr_bzs, dep_bzs = common.extract_all_bzs(commit.message,
                                                                    bugzilla_ids,
                                                                    bz_dependencies)
        # pylint: disable=broad-except
        except Exception:
            found_bzs = []
            non_mr_bzs = []

        for bug in found_bzs:
            review_lists[bug] = build_review_lists(commit, review_lists, bug)

        for bug in non_mr_bzs:
            review_lists[bug] = build_review_lists(commit, review_lists, bug)

        for bug in dep_bzs:
            review_lists[bug] = build_review_lists(commit, review_lists, bug)

        if not found_bzs and not non_mr_bzs and not dep_bzs:
            review_lists["-"] = build_review_lists(commit, review_lists, "-")

    (notes, tgt) = validate_bzs(project, merge_request, bugzilla_ids, bz_dependencies,
                                review_lists, bzcon)
    (table, approved) = get_report_table(review_lists)
    trim_notes(notes)

    return (notes, tgt, table, approved)


def run_bz_validation(project, merge_request, force_show_summary):
    # pylint: disable=too-many-locals
    """Perform the Bugzilla validation for a merge request."""
    # If we do not connect to bugzilla then we cannot fully validate BZ readiness.
    bzcon = common.try_bugzilla_conn()
    bugzilla_ids = common.extract_bzs(merge_request.description)
    bz_dependencies = common.extract_dependencies(merge_request.description)
    LOGGER.info("Running BZ validation for merge request: iid=%s, description=%s, bz=%s, deps=%s",
                merge_request.iid, merge_request.description, bugzilla_ids, bz_dependencies)

    (notes, bz_target, table, approved) = check_on_bzs(project, merge_request,
                                                       bugzilla_ids,
                                                       bz_dependencies, bzcon)

    if approved is BZState.READY_FOR_MERGE:
        status = common.READY_SUFFIX
        label_color = common.READY_LABEL_COLOR
        label_description = BZ_READY_FOR_MERGE_LABEL
    elif approved is BZState.READY_FOR_QA:
        status = common.NEEDS_TESTING_SUFFIX
        label_color = '#CAC542'
        label_description = BZ_READY_FOR_QA_LABEL
    else:
        status = common.NEEDS_REVIEW_SUFFIX
        label_color = common.NEEDS_REVIEW_LABEL_COLOR
        label_description = BZ_NEEDS_REVIEW_LABEL
    label = common.create_label_object(f'Bugzilla::{status}', label_color, label_description)
    label_changed = common.add_label_to_merge_request(project, merge_request.iid, [label])

    if not misc.is_production():
        LOGGER.info(print_text_report(merge_request, notes, bz_target, table, approved))

    if misc.is_production() and ((label_changed and not approved) or force_show_summary):
        report = print_gitlab_report(merge_request, notes, bz_target, table, approved)
        merge_request.notes.create({'body': report})

    cdlib.set_dependencies_label(project, merge_request)


def bz_process_mr(gl_instance, message):
    """Process a merge request message."""
    project = gl_instance.projects.get(message.payload["project"]["id"])
    merge_request = project.mergerequests.get(message.payload["object_attributes"]["iid"])
    run_bz_validation(project, merge_request, False)


def bz_process_note(gl_instance, message):
    """Process a note message."""
    LOGGER.debug("Checking note request\n")
    if "merge_request" not in message.payload:
        return

    force_eval = common.force_webhook_evaluation(message.payload['object_attributes']['note'], 'bz')
    if not force_eval:
        return

    project = gl_instance.projects.get(message.payload["project"]["id"])
    merge_request = project.mergerequests.get(message.payload["merge_request"]["iid"])
    run_bz_validation(project, merge_request, True)


WEBHOOKS = {
    "merge_request": bz_process_mr,
    "note": bz_process_note,
}


def main(args):
    """Run main loop."""
    parser = common.get_arg_parser('BUGZILLA')
    args = parser.parse_args(args)
    common.generic_loop(args, WEBHOOKS)


if __name__ == "__main__":
    main(sys.argv[1:])
