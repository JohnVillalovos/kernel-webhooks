"""Common commit/dependency library code that can be used by all webhooks."""
import difflib
import os
import re

from cki_lib import logger
import git
from gitdb.exc import BadName
from gitdb.exc import BadObject

from . import common

LOGGER = logger.get_logger(__name__)


def extract_files(commit):
    """Extract the list of files from the commit."""
    filelist = []
    diffs = commit.diff()
    LOGGER.debug(diffs)
    for diff in diffs:
        filelist.append(diff['new_path'])
    LOGGER.debug(filelist)
    return filelist


def search_compiled(line, re_list, group0=False, first=True):
    """Actual regex matching happens here."""
    matches = []
    for item in re_list:
        match = item.search(line)
        if match:
            if group0 and match.group(0):
                matches.append(match.group(0))
            elif not group0 and match.group(1):
                matches.append(match.group(1))
            if first:
                break

    return matches


def get_partial_diff(diff, filelist):
    """Extract partial diff, used for partial backport comparisons."""
    # this is essentially a reimplementation of the filterdiff binary in python

    in_wanted_file = False
    in_header = True
    hit = False
    partial_diff = ""
    header = []
    new_diff = []
    cache = []

    for line in diff.split('\n'):
        if line.startswith("diff --git "):
            in_wanted_file = False
            for path in filelist:
                if path in line:
                    in_wanted_file = True
                    continue

        if not in_wanted_file:
            continue

        # save the header stuff in case there is stuff to print
        if in_header:
            if line[0:2] == "@@":
                in_header = False
            else:
                header.append(line)
                continue

        # hit boundary, save cached patchlet, if it wasn't filtered
        if line[0:2] == "@@":
            if hit:
                # only print header if there is a hit
                new_diff.extend(header)
                new_diff.extend(cache)
                hit = False
                header = []

            # reset cache and start on new patchlet
            cache = [line]
            continue

        # auto save line
        cache.append(line)

        # nothing got flagged, must be important
        hit = True

    # flush final piece
    if hit:
        new_diff.extend(header)
        new_diff.extend(cache)

    for line in new_diff:
        partial_diff += "%s\n" % line

    return partial_diff


def filter_diff(diff):
    """Filter the diff, isolating only the kinds of differences we care about."""
    # this works by going through each patchlet and filtering out as
    # much junk as possible.  If anything is left, consider it a hit and
    # save the _whole_ patchlet for later.  Proceed to next patchlet.

    in_header = True
    hit = False
    header = []
    new_diff = []
    cache = []
    diff_compiled = []

    # sort through rules to see if this chunk is worth saving
    # most of the filters are patch header junk.  Don't care if those
    # offsets are different.
    # The last filter is the most interesting: filter context differences
    # Basically that just filters out noise that changed around
    # the patch and not the parts of the patch that does anything.
    # IOW, the lines in a patch with _no_ ± in front.
    # Personal preference if that is interesting or not.

    diff_re = ["^[+|-]index ",
               "^[+|-]--- ",
               r"^[+|-]\+\+\+",
               "^[+|-]diff ",
               "^[+|-]$",
               "^[+|-]@@ ",
               "^[+|-]new file mode ",
               "^[+|-]deleted file mode ",
               "^[+|-] "]

    for prefix in diff_re:
        diff_compiled.append(re.compile(prefix))

    # end pre-compile stuff

    for line in diff:

        # save the header stuff in case there is stuff to print
        if in_header:
            if line[0:2] == "@@":
                in_header = False
            else:
                header.append(line)
                continue

        # hit boundary, save cached patchlet, if it wasn't filtered
        if line[0:2] == "@@":
            if hit:
                # only print header if there is a hit
                new_diff.extend(header)
                new_diff.extend(cache)
                hit = False
                header = []

            # reset cache and start on new patchlet
            cache = [line]
            continue

        # auto save line
        cache.append(line)

        # filter lines
        if line[0] == " ":
            # patch context, ignore
            continue
        if search_compiled(line, diff_compiled, group0=True):
            # hit junk, skip
            continue

        # nothing got flagged, must be important
        hit = True

    # flush final piece
    if hit:
        new_diff.extend(header)
        new_diff.extend(cache)

    return new_diff


def get_submitted_diff(commit):
    """Extract diff for submitted patch."""
    diff = ""
    filelist = []

    for path in commit:
        diff += f"diff --git a/{path['old_path']} b/{path['new_path']}\n"
        if path['new_file']:
            diff += f"new file mode {path['b_mode']}\n"
        if path['renamed_file']:
            diff += f"rename from {path['old_path']}\n"
            diff += f"rename to {path['new_path']}\n"
        if path['deleted_file']:
            diff += f"deleted file mode {path['a_mode']}\n"
        diff += f"index blahblah..blahblah {path['b_mode']}\n"
        if path['new_file']:
            diff += "--- /dev/null\n"
        else:
            diff += f"--- a/{path['old_path']}\n"
        if path['deleted_file']:
            diff += "+++ /dev/null\n"
        else:
            diff += f"+++ b/{path['new_path']}\n"
        diff += path['diff']
        if path['old_path'] not in filelist:
            filelist.append(path['old_path'])
        if path['new_path'] not in filelist:
            filelist.append(path['new_path'])

    return (diff, filelist)


def compare_commits(adiff, bdiff):
    """Perform interdiff of two patches' diffs."""
    interdiff = difflib.unified_diff(adiff.split('\n'), bdiff.split('\n'),
                                     fromfile='diff_a', tofile='diff_b', lineterm="")
    interesting = filter_diff(interdiff)
    return interesting


def mr_get_diff_ids(mrequest):
    """Get the sorted list of diff ids in the MR."""
    diffs = mrequest.diffs.list()
    diff_ids = []
    for diff in diffs:
        diff_ids.append(diff.id)
    diff_ids.sort()
    return diff_ids


def mr_get_last_two_diff_ranges(mrequest, diff_ids):
    """Get the head and start sha plus created_at for the latest two diffs in MR."""
    old = {'head': '', 'start': '', 'created': ''}
    latest = {'head': '', 'start': '', 'created': ''}
    for diff_id in diff_ids:
        diff = mrequest.diffs.get(diff_id)
        old['head'] = latest['head']
        old['start'] = latest['start']
        old['created'] = latest['created']
        latest['head'] = diff.head_commit_sha
        latest['start'] = diff.start_commit_sha
        latest['created'] = diff.created_at

    return (latest, old)


def mr_dependencies(mrequest):
    """Check if the current MR revision has dependencies or not."""
    labels = mrequest.labels
    for label in labels:
        if label.startswith("Dependencies::"):
            dep_scope = label.split("::")[-1]
            if dep_scope not in common.READY_SUFFIX:
                return dep_scope

    return None


def mr_get_latest_start_sha(mrequest, start_sha):
    """Get the latest Dependencies:: label, if present."""
    deps = mr_dependencies(mrequest)
    if deps is None:
        return start_sha

    return deps


def is_first_dep(commit, dep_sha):
    """Return true if this commit matches the Dependencies:: label's sha."""
    return commit.id.startswith(dep_sha)


def get_dependencies_data(mrequest):
    """Figure out if we have dependencies, and if so, where they start."""
    has_deps = False
    start_sha = mrequest.diff_refs['start_sha']
    dep_sha = mr_get_latest_start_sha(mrequest, start_sha)
    if start_sha != dep_sha:
        has_deps = True

    return (has_deps, dep_sha)


def mr_get_old_start_sha(mrequest, latest, old):
    """Get the starting sha for old MR diff."""
    label_events = mrequest.resourcelabelevents.list(all=True)
    start_sha = old['start']
    prior_label_exists = False
    for evt in label_events:
        evtid = mrequest.resourcelabelevents.get(evt.id)
        if evtid.label and evtid.label['name'].startswith("Dependencies") and \
           evtid.action == "remove":
            dep_scope = evtid.label['name'].split("::")[-1]
            evt_at = evtid.created_at
            if dep_scope not in common.READY_SUFFIX:
                start_sha = dep_scope
                prior_label_exists = True
            # likely improperly defined deps at prior rev
            elif dep_scope in common.READY_SUFFIX and str(evt_at) > old['created']:
                start_sha = latest['start']
                prior_label_exists = True

    if prior_label_exists:
        return start_sha

    return mr_get_latest_start_sha(mrequest, start_sha)


def get_diff_from_mr(mrequest, diff_ids, rev):
    """Extract an MR diff from the MR api."""
    rev_diff = None
    for diff_id in diff_ids:
        diff = mrequest.diffs.get(diff_id)
        if diff.head_commit_sha == rev['head']:
            rev_diff = get_submitted_diff(diff.diffs)

    if rev_diff is not None:
        return rev_diff[0]

    return None


def get_git_diff(repo, rev):
    """Extract an MR diff from git."""
    LOGGER.info('Generating diff: %s..%s, %s', rev['start'], rev['head'], rev['created'])

    try:
        rev_start = repo.commit(rev['start'])
        rev_head = repo.commit(rev['head'])
    except (BadObject, BadName, ValueError) as err:
        LOGGER.warning("Upstream gitref not found, aborting check - (%s)", err)
        return None

    rev_diff = repo.git.diff(rev_start, rev_head)

    return rev_diff


def mr_code_changed(mrequest, rhkernel_src):
    """Check to see if code in this MR actually changed."""
    if not os.path.exists(rhkernel_src):
        LOGGER.warning("No git tree exists at %s, assume code changed", rhkernel_src)
        return True

    # We run this before any other checks, to make sure we fetch all refs for
    # future use when the current revision has a Depedencies::<sha> label
    repo = git.Repo(rhkernel_src)
    mr_path = mrequest.references['full'].split("/")[-1].split("!")[0]
    if mr_dependencies(mrequest) is not None:
        for remote in repo.remotes:
            if mr_path in remote.name:
                LOGGER.info("Updating refs for remote %s", remote)
                remote.fetch()

    diff_ids = mr_get_diff_ids(mrequest)

    # If there is only one diff ID, code hasn't changed
    if len(diff_ids) < 2:
        LOGGER.info("No v2+ set found.")
        return False

    (latest, old) = mr_get_last_two_diff_ranges(mrequest, diff_ids)
    orig_latest_start = latest['start']
    orig_old_start = old['start']

    latest['start'] = mr_get_latest_start_sha(mrequest, latest['start'])
    old['start'] = mr_get_old_start_sha(mrequest, latest, old)

    # Check if we can skip fetching git diffs and just pull diffs from MR
    if latest['start'] == orig_latest_start:
        LOGGER.info("Fetching latest diff from MR %s.", mrequest.iid)
        new_diff = get_diff_from_mr(mrequest, diff_ids, latest)
    else:
        LOGGER.info("Fetching latest diff for MR %s from git.", mrequest.iid)
        new_diff = get_git_diff(repo, latest)

    if old['start'] == orig_old_start:
        LOGGER.info("Fetching prior diff from MR %s.", mrequest.iid)
        old_diff = get_diff_from_mr(mrequest, diff_ids, old)
    else:
        LOGGER.info("Fetching prior diff for MR %s from git.", mrequest.iid)
        old_diff = get_git_diff(repo, old)

    if old_diff is None or new_diff is None:
        LOGGER.info("Unable to find one or more diffs, assume code changed")
        return True

    diff_diffs = compare_commits(old_diff, new_diff)
    if not diff_diffs:
        LOGGER.info("No code changes found after update.")
        return False

    LOGGER.info("Code changed after update.")
    return True


def get_filtered_changed_files(gl_project, gl_mergerequest):
    """Walk all commits, excluding Dependencies, and build changed files list."""
    filelist = []

    diff_ids = mr_get_diff_ids(gl_mergerequest)
    for diff_id in diff_ids:
        diff = gl_mergerequest.diffs.get(diff_id)
        start_sha = diff.start_commit_sha

    start_sha = mr_get_latest_start_sha(gl_mergerequest, start_sha)

    for commit in gl_mergerequest.commits():
        commit_ref = gl_project.commits.get(commit.id)
        # skip merge commits
        if len(commit_ref.parent_ids) > 1:
            continue
        if commit.id.startswith(start_sha):
            break
        diffs = commit.diff()
        for diff in diffs:
            filelist.append(diff['new_path'])

    filelist = set(filelist)
    filelist = list(filelist)
    filelist.sort()

    return filelist


def get_dependencies_label_scope(depcommit):
    """Set Dependencies:: label with scope of either OK or the sha of first dependent commit."""
    if depcommit is not None:
        scope = depcommit[:12]
    else:
        scope = common.READY_SUFFIX

    return scope


def find_first_dependency_commit(merge_request):
    """Find the first commit in the MR that is for a Dependency."""
    dependencies = common.extract_dependencies(merge_request.description)
    if len(dependencies) == 0:
        return None

    for commit in merge_request.commits():
        bugs = common.extract_bzs(commit.message)
        for bug in bugs:
            if bug in dependencies:
                return commit.id

    return None


def set_dependencies_label(project, merge_request):
    """Set the Dependencies:: label for the given merge request."""
    depcommit = find_first_dependency_commit(merge_request)
    scope = get_dependencies_label_scope(depcommit)

    if scope == common.READY_SUFFIX:
        label_color = common.READY_LABEL_COLOR
        label_description = common.DEP_READY_LABEL
    else:
        label_color = common.NEEDS_REVIEW_LABEL_COLOR
        label_description = common.DEP_NEEDS_REVIEW_LABEL

    label_name = f'Dependencies::{scope}'
    label = common.create_label_object(label_name, label_color, label_description)
    common.add_label_to_merge_request(project, merge_request.iid, [label])
    return label_name
