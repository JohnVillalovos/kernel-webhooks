"""Common code that can be used by all webhooks."""
import argparse
import enum
import json
import os
import pathlib
import re
import sys
from urllib import parse

import bugzilla
from cki_lib import logger
from cki_lib import misc
from cki_lib.gitlab import get_instance
from cki_lib.messagequeue import Message
from cki_lib.messagequeue import MessageQueue
from gitlab.exceptions import GitlabCreateError
import sentry_sdk

from . import owners

LOGGER = logger.get_logger(__name__)


def get_arg_parser(webhook_prefix):
    """Intialize a commandline parser.

    Returns: argparse parser.
    """
    parser = argparse.ArgumentParser(
        description='Manual handling of merge requests')
    parser.add_argument('--merge-request',
                        help='Process given merge request URL only')
    parser.add_argument('--action', default='',
                        help='Action for the MR when using URL only')
    parser.add_argument('--json-message-file', default='',
                        help='Process a single JSON message in a file')
    parser.add_argument('--oldrev', action='store_true',
                        help='Treat this as changed MR when using URL only')
    parser.add_argument('--note',
                        help='Process a note for the given merge request')
    parser.add_argument('--dont-ignore-self', action='store_false',
                        help="Don't ignore messages from self")
    parser.add_argument('--sentry-ca-certs', default=os.getenv('REQUESTS_CA_BUNDLE'),
                        help='An optional path to an alternative CA bundle file in PEM-format.')
    parser.add_argument('--rabbitmq-host', default=os.environ.get('RABBITMQ_HOST', 'localhost'))
    parser.add_argument('--rabbitmq-port', type=int,
                        default=misc.get_env_int('RABBITMQ_PORT', 5672))
    parser.add_argument('--rabbitmq-user', default=os.environ.get('RABBITMQ_USER', 'guest'))
    parser.add_argument('--rabbitmq-password',
                        default=os.environ.get('RABBITMQ_PASSWORD', 'guest'))
    parser.add_argument('--rabbitmq-exchange',
                        default=os.environ.get('WEBHOOK_RECEIVER_EXCHANGE', 'cki.webhooks'))
    parser.add_argument('--rabbitmq-routing-key',
                        default=os.environ.get(f'{webhook_prefix}_ROUTING_KEYS'),
                        help='RabbitMQ routing key. Required when processing queue.')
    parser.add_argument('--rabbitmq-queue-name',
                        default=os.environ.get(f'{webhook_prefix}_QUEUE'),
                        help='RabbitMQ queue name. Required when processing queue.')

    return parser


def parse_mr_url(url):
    """Parse the merge request URL used for manual handlers.

    Args:
        url: Full merge request URL.

    Returns:
        A tuple of (gitlab_instance, mr_object, project_path_with_namespace).
    """
    url_parts = parse.urlsplit(url)
    instance_url = parse.urlunparse(url_parts[:2] + ('',) * 4)
    gl_instance = get_instance(instance_url)
    match = re.match(r'/(.*)/merge_requests/(\d+)', url_parts.path)
    project_path = re.sub('/-$', '', match[1])
    gl_project = gl_instance.projects.get(project_path)
    gl_mergerequest = gl_project.mergerequests.get(int(match[2]))

    return gl_instance, gl_project, gl_mergerequest, project_path


def mr_is_closed(merge_request):
    """We really don't want to run on closed MRs, add common check function."""
    return merge_request.state == "closed"


# pylint: disable=unused-argument
def process_message(routing_key, payload, webhooks, ignore_msgs_from_self, **kwargs):
    """Process a webhook message."""
    object_kind = payload.get('object_kind')
    if not object_kind or object_kind not in webhooks:
        LOGGER.info('Ignoring message from queue: %s', json.dumps(payload, indent=None))
        return False  # unit tests

    message = Message(payload)
    if 'state' in message.payload and message.payload['state'] == 'closed':
        LOGGER.debug("Ignoring event with 'closed' state.")
        return False

    if object_kind == 'note' and \
       message.payload['object_attributes']['noteable_type'] != 'MergeRequest':
        LOGGER.info('Only processing notes related to merge requests: %s',
                    json.dumps(payload, indent=None))
        return False

    with message.gl_instance() as gl_instance:
        if not hasattr(gl_instance, 'user'):
            gl_instance.auth()

        if ignore_msgs_from_self:
            if gl_instance.user.username == message.payload['user']['username']:
                LOGGER.info('Ignoring bot message from queue: %s', json.dumps(payload, indent=None))
                return False

        LOGGER.info('Processing message from queue: %s', json.dumps(payload, indent=None))
        webhooks[object_kind](gl_instance, message, **kwargs)

    return True  # unit tests


def consume_queue_messages(args, webhooks, ignore_msgs_from_self, **kwargs):
    """Begin processing the main loop by reading messages from the queue."""
    if args.json_message_file:
        # Only process a single message
        msg_json = pathlib.Path(args.json_message_file).read_text()
        msg = json.loads(msg_json)
        process_message(None, msg, webhooks, ignore_msgs_from_self, **kwargs)
        return

    if not args.rabbitmq_queue_name or not args.rabbitmq_routing_key:
        LOGGER.error('The arguments --rabbitmq-queue-name and --rabbitmq-routing-key must be')
        LOGGER.error('specified in order to process the queue. Hint: You may want to process ')
        LOGGER.error('a single merge request with the --merge-request argument.')
        sys.exit(1)

    queue = MessageQueue(host=args.rabbitmq_host, port=args.rabbitmq_port, user=args.rabbitmq_user,
                         password=args.rabbitmq_password)

    misc.sentry_init(sentry_sdk, ca_certs=args.sentry_ca_certs)

    queue.consume_messages(args.rabbitmq_exchange, args.rabbitmq_routing_key.split(),
                           lambda routing_key, payload: process_message(
                               routing_key, payload, webhooks, ignore_msgs_from_self,
                               **kwargs
                           ), args.rabbitmq_queue_name)


def get_argparse_environ_opts(key):
    """Read default value for argparse from an environment variable if present."""
    val = os.environ.get(key)
    return {'default': val} if val else {'required': True}


def print_notes(notes):
    """Print the notes section of upstream commit ID report."""
    report = ""
    noteid = 0
    while noteid < len(notes):
        report += f"{noteid + 1}. "
        if notes[noteid] is None:
            noteid += 1
            continue
        report += notes[noteid].rstrip("\n").replace("\n", "\n   ")
        report += "\n"
        noteid += 1
    report += "\n" if report else ""
    report = report.replace("<", "&lt;")
    report = report.replace(">", "&gt;")
    report = report.replace("\n   ", "<br>&emsp;")
    return report


def make_payload(url, kind):
    """Create a fake payload dict."""
    payload = {'object_kind': kind,
               'object_attributes': {},
               'project': {},
               'changes': {},
               'user': {},
               'state': 'opened',
               'labels': [],
               '_mr_id': None}
    url_parts = parse.urlsplit(url)
    match = re.match(r'/(.*)/merge_requests/(\d+)', url_parts.path)
    prj_id = re.sub('/-$', '', match[1])
    payload["project"]["id"] = prj_id
    web_url = "%s://%s/%s" % (url_parts.scheme, url_parts.netloc, prj_id)
    payload["project"]["web_url"] = web_url
    payload["user"]["username"] = "cli"
    if kind == "note":
        payload['merge_request'] = {'iid': int(match[2])}
        payload["object_attributes"]["noteable_type"] = "MergeRequest"
    elif kind == "merge_request":
        payload["object_attributes"]["iid"] = int(match[2])
        payload["object_attributes"]["work_in_progress"] = False
    elif kind == "pipeline":
        payload['merge_request'] = {'iid': int(match[2])}
    else:
        payload["_mr_id"] = int(match[2])
    return payload


def process_mr_url(mr_url, action, note):
    """Create a fake payload for the given mr/note."""
    if action == 'note':
        payload = make_payload(mr_url, 'note')
        payload["object_attributes"]["note"] = note
    elif action == 'merge_request':
        payload = make_payload(mr_url, 'merge_request')
        payload['object_attributes']['action'] = 'open'
    elif action == 'pipeline':
        payload = make_payload(mr_url, 'pipeline')
    return payload


def generic_loop(args, hook_handlers, ignore_msgs_from_self=True, **kwargs):
    """Run hook loop."""
    if args.merge_request:
        if not args.action:
            if args.note:
                action = 'note'
            else:
                action = 'merge_request'
        else:
            action = args.action
        payload = process_mr_url(args.merge_request, action, args.note)
        process_message('cmdline', payload, hook_handlers, ignore_msgs_from_self, **kwargs)
        return
    consume_queue_messages(args, hook_handlers, ignore_msgs_from_self, **kwargs)


def commits_have_not_changed(payload):
    """Return True if the commits in the MR have not changed."""
    action = payload['object_attributes']['action']
    if action == 'update' and 'oldrev' not in payload['object_attributes']:
        return True
    return False


def mr_action_affects_commits(message):
    """Return True if the message indicates there has been any change to the MR's commits."""
    action = message.payload['object_attributes']['action']

    # Check to see if the target branch was changed.
    if action == 'update' and 'changes' in message.payload and \
       'merge_status' in message.payload['changes']:
        return True

    # True if action is 'open' or, action is 'update' and 'oldrev' is set.
    if commits_have_not_changed(message.payload):
        LOGGER.debug("Ignoring MR \'update\' action without an oldrev.")
        return False
    if action not in ('update', 'open'):
        LOGGER.debug("Ignoring MR action '%s'.", action)
        return False
    return True


def build_note_string(notes):
    """Build note string for report table."""
    notestr = ", ".join(notes)
    notestr = "See " + notestr + "|\n" if notestr else "-|\n"
    return notestr


def build_commits_for_row(row):
    """Build list of commits for a row in report table."""
    commits = row[1] if len(row[1]) < 2 else row[1][:2] + ["(...)"]
    count = 0
    while count < len(commits):
        commits[count] = commits[count][:8]
        count += 1
    return commits


NEEDS_REVIEW_LABEL_COLOR = '#FF0000'
NEEDS_TESTING_LABEL_COLOR = '#FF0000'
READY_LABEL_COLOR = '#428BCA'
NEEDS_REVIEW_SUFFIX = 'NeedsReview'
NEEDS_TESTING_SUFFIX = 'NeedsTesting'
TESTING_FAILED_SUFFIX = 'TestingFailed'
READY_SUFFIX = 'OK'

BASE_DEPENDENCIES = ['Acks::OK',
                     'CommitRefs::OK',
                     'Dependencies::OK',
                     'Signoff::OK']

READY_FOR_MERGE_DEPS = BASE_DEPENDENCIES + ['Bugzilla::OK']
READY_FOR_QA_DEPS = BASE_DEPENDENCIES + ['Bugzilla::NeedsTesting']

READY_FOR_MERGE_LABEL = \
    {'name': 'readyForMerge',
     'color': '#8BCA42',
     'description': ('All automated checks pass, this merge request should be suitable for '
                     'inclusion in main now.')
     }

READY_FOR_QA_LABEL = \
    {'name': 'readyForQA',
     'color': NEEDS_TESTING_LABEL_COLOR,
     'description': ('Basic checks pass, this merge request should be suitable for '
                     'testing by QA now.')
     }

DEP_READY_LABEL = ("This merge request does not contain any dependencies that aren't already"
                   " merged into the main tree.")
DEP_NEEDS_REVIEW_LABEL = ('This merge request depends on code which does not exist in the'
                          ' target branch.')


def create_label_object(name, color, description):
    """Return an object ready to pass to add_label_to_merge_request in a list."""
    return {'name': name, 'color': color, 'description': description}


def _create_project_label(gl_project, label):
    """Create a new label on the project."""
    LOGGER.info('Creating label %s on project.', label)
    if misc.is_production():
        try:
            gl_project.labels.create(label)
        except GitlabCreateError as err:
            if err.response_code == 409 and "Label already exists" in err.error_message:
                LOGGER.info('%s: %s.', err.error_message, label['name'])
            else:
                raise


def _edit_project_label(gl_project, existing_label, new_label):
    """Check if a project label needs updating. Creates the label if it does not exist."""
    if existing_label:
        # If the label exists then confirm the existing properties match the new label values.
        label_changed = False
        for item in new_label:
            if new_label[item] != getattr(existing_label, item):
                setattr(existing_label, item, new_label[item])
                label_changed = True
        if label_changed:
            LOGGER.info('Editing label %s on project.', existing_label.name)
            if misc.is_production():
                existing_label.save()
    else:
        _create_project_label(gl_project, new_label)


def _match_label(project, target_label, label_list=None):
    """Return the ProjectLabel object whose name matches the target."""
    if not label_list:
        label_list = project.labels.list(search=target_label)
    return next((label for label in label_list if label.name == target_label), None)


def _add_label_quick_actions(gl_project, label_list):
    # Use /label quick action to add the label to the merge request. This requires ensuring that
    # the label is available on the project.
    label_cmds = []

    # If we're only operating on a few labels then don't bother downloading
    # the project's entire label list, just search for them one at a time in _match_label().
    all_labels = gl_project.labels.list(all=True) if len(label_list) >= 5 else None
    for label in label_list:
        existing_label = _match_label(gl_project, label['name'], all_labels)
        _edit_project_label(gl_project, existing_label, label)
        label_cmds.append('/label "%s"' % label['name'])

    return label_cmds


def _find_extra_required_labels(labels):
    extra_required_labels = []
    for label in labels:
        if label.split("::")[-1] == NEEDS_TESTING_SUFFIX:
            proposed_extra_label = f'{label.split("::")[0]}::{READY_SUFFIX}'
            if proposed_extra_label not in READY_FOR_MERGE_DEPS:
                extra_required_labels.append(proposed_extra_label)
    LOGGER.debug("Extra required labels: %s", extra_required_labels)
    return extra_required_labels


def _compute_mr_status_labels(gl_project, gl_mergerequest, label_cmds):
    required_labels = set(READY_FOR_MERGE_DEPS)
    required_labels.update(_find_extra_required_labels(gl_mergerequest.labels))
    if required_labels.issubset(set(gl_mergerequest.labels)):
        if READY_FOR_MERGE_LABEL['name'] not in gl_mergerequest.labels:
            label_cmds += _add_label_quick_actions(gl_project, [READY_FOR_MERGE_LABEL])
            label_cmds.append('/unlabel "%s"' % READY_FOR_QA_LABEL['name'])
    elif set(READY_FOR_QA_DEPS).issubset(set(gl_mergerequest.labels)):
        if READY_FOR_QA_LABEL['name'] not in gl_mergerequest.labels:
            label_cmds += _add_label_quick_actions(gl_project, [READY_FOR_QA_LABEL])
            label_cmds.append('/unlabel "%s"' % READY_FOR_MERGE_LABEL['name'])
    elif READY_FOR_MERGE_LABEL['name'] in gl_mergerequest.labels \
            or READY_FOR_QA_LABEL['name'] in gl_mergerequest.labels:
        label_cmds.append('/unlabel "%s"' % READY_FOR_MERGE_LABEL['name'])
        label_cmds.append('/unlabel "%s"' % READY_FOR_QA_LABEL['name'])


def _run_label_commands(gl_project, gl_mergerequest, label_cmds):
    _compute_mr_status_labels(gl_project, gl_mergerequest, label_cmds)

    if label_cmds:
        LOGGER.info('Modifying labels on merge request %s: %s', gl_mergerequest.iid,
                    ' '.join(label_cmds))
        if misc.is_production():
            try:
                gl_mergerequest.notes.create({'body': '\n'.join(label_cmds)})
            except GitlabCreateError as err:
                if err.response_code == 400 and "can't be blank" in err.error_message:
                    LOGGER.exception('Unexpected Gitlab response when creating quick action note.')
                else:
                    raise

        return True

    LOGGER.info('No labels to change on merge request %s', gl_mergerequest.iid)
    return False


def _filter_mr_labels(merge_request, label_list):
    """Remove existing scoped labels that match list and add new labels. Return the new list."""
    # If this is a scoped label, then remove the old value from the mr object list so that the
    # readyForMerge label is added or removed appropriately. Support nested scoped labels like
    # BZ::123::OK in the prefix variable.
    filtered_labels = []
    to_remove = []
    for label in label_list:
        if label['name'] not in merge_request.labels:
            if '::' in label['name']:
                prefix = '::'.join(label['name'].split('::')[0:-1])
                to_remove += [x for x in merge_request.labels if x.startswith(f'{prefix}::')]
            merge_request.labels.append(label['name'])
            filtered_labels.append(label)
    merge_request.labels = [x for x in merge_request.labels if x not in to_remove]
    return filtered_labels


def add_label_to_merge_request(gl_project, mr_id, label_input):
    """Add labels to a GitLab merge request.

    Args:
        gl_project: Project object as returned by the gitlab module.
        mr_id: The ID of the MR to add the label(s) to.
        label_input: A List containing at least one dict describing a label. See
                     create_label_object().

    Returns:
        True if any labels on the given MR changed.
        False if there was no change to the MR's labels.
    """
    LOGGER.info('Evaluating label %s for addition to MR %s', label_input, mr_id)
    # Some of the webhooks can take several minutes to run. To help avoid collisions with the
    # other webhooks, fetch the most recent version of the merge request to get the latest labels.
    gl_mergerequest = gl_project.mergerequests.get(mr_id)

    filtered_labels = _filter_mr_labels(gl_mergerequest, label_input)
    label_cmds = _add_label_quick_actions(gl_project, filtered_labels)

    return _run_label_commands(gl_project, gl_mergerequest, label_cmds)


def remove_labels_from_merge_request(gl_project, mr_id, labels):
    """Remove a label on a GitLab merge request."""
    # Some of the webhooks can take several minutes to run. To help avoid collisions with the
    # other webhooks, fetch the most recent version of the merge request to get the latest labels.
    gl_mergerequest = gl_project.mergerequests.get(mr_id)

    label_cmds = []
    for label in labels:
        if label in gl_mergerequest.labels:
            label_cmds.append(f'/unlabel "{label}"')
            gl_mergerequest.labels.remove(label)

    return _run_label_commands(gl_project, gl_mergerequest, label_cmds)


class LabelPart(enum.IntEnum):
    """Part of label to look at."""

    FULL = 0
    PREFIX = 1
    SUFFIX = 2


def required_label_removed(payload, suffix, changed_labels):
    """Check if an extra required label was removed."""
    no_commit_changes = commits_have_not_changed(payload)
    for label in changed_labels:
        if not label.endswith(suffix):
            continue
        # Don't act on the base ready labels, let their hooks handle them
        if label in READY_FOR_MERGE_DEPS:
            continue
        prefix = label.split("::")[0]
        if suffix == NEEDS_TESTING_SUFFIX:
            if f'{prefix}::{READY_SUFFIX}' not in changed_labels and \
               f'{prefix}::{TESTING_FAILED_SUFFIX}' not in changed_labels and \
               no_commit_changes:
                return True
        elif suffix == READY_SUFFIX:
            if f'{prefix}::{NEEDS_TESTING_SUFFIX}' not in changed_labels and \
               f'{prefix}::{TESTING_FAILED_SUFFIX}' not in changed_labels and \
               no_commit_changes:
                return True
        elif suffix == TESTING_FAILED_SUFFIX:
            if f'{prefix}::{READY_SUFFIX}' not in changed_labels and \
               f'{prefix}::{NEEDS_TESTING_SUFFIX}' not in changed_labels and \
               no_commit_changes:
                return True
    return False


def has_label_changed(msg, label_name, part):
    """Check to see if the given MR event Message indicates the given label changed.

    Args:
        msg: A cki_lib Message payload dict.
        label_name: Name of the label to check for.
        part: Whether label_name is a full label, a prefix or a suffix.

    Returns:
        True if the label has changed.
        False if the label has not changed.
    """
    if 'labels' not in msg['changes']:
        return False

    prev_labels = {item['title'] for item in msg['changes']['labels']['previous']}
    cur_labels = {item['title'] for item in msg['changes']['labels']['current']}

    changed_labels = set()
    changed_labels.update(prev_labels.difference(cur_labels))
    changed_labels.update(cur_labels.difference(prev_labels))

    if part == LabelPart.FULL and label_name in changed_labels:
        return True
    if part == LabelPart.PREFIX and [label for label in changed_labels
                                     if label.startswith(label_name)]:
        return True
    if part == LabelPart.SUFFIX:
        return required_label_removed(msg, label_name, changed_labels)
    return False


def force_webhook_evaluation(notetext, webhook_name):
    """Check to see if the note text requested a evaluation from the webhook."""
    return notetext.startswith('request-evaluation') or \
        notetext.startswith(f'request-{webhook_name}-evaluation')


def try_bugzilla_conn():
    """If BUGZILLA_API_KEY is set then try to return a bugzilla connection object."""
    if not os.environ.get('BUGZILLA_API_KEY'):
        LOGGER.info("No bugzilla API key, not connecting to bugzilla.")
        return False
    return connect_bugzilla(os.environ.get('BUGZILLA_API_KEY'))


def connect_bugzilla(api_key, cookie_file=None, token_file=None):
    """Connect to bugzilla and return a bugzilla connection object."""
    try:
        # See https://github.com/python-bugzilla/python-bugzilla/blob/master/bugzilla/base.py#L175
        bzcon = bugzilla.Bugzilla('bugzilla.redhat.com', api_key=api_key,
                                  cookiefile=cookie_file, tokenfile=token_file)
    except ConnectionError:
        LOGGER.exception("Problem connecting to bugzilla server.")
        return False
    except PermissionError:
        LOGGER.exception("Problem with file permissions for bugzilla connection.")
        return False
    return bzcon


def find_bz_in_line(line, prefix):
    """Return bug number from properly formated Bugzilla: line."""
    # BZs must be called out one-per-line, begin with f'{prefix}:' and contain a complete BZ URL.
    # Plus an exception for INTERNALs.
    line = line.rstrip()
    pattern = prefix + r': http(s)?://bugzilla\.redhat\.com/(show_bug\.cgi\?id=)?(?P<bug>\d{4,8})$'
    bznum_re = re.compile(pattern)
    bugs = bznum_re.match(line)
    if bugs:
        return bugs.group('bug')
    if line == prefix + ': INTERNAL':
        return 'INTERNAL'
    return None


def get_owners_parser(owners_yaml):
    """Return a parser for the owners.yaml to lookup kernel subsystem information."""
    return owners.Parser(pathlib.Path(owners_yaml).read_text())


def extract_all_bzs(message, mr_bugs, dependencies):
    """Extract all BZs from the message."""
    bzs = []
    non_mr_bzs = []
    dep_bzs = []

    if message:
        mlines = message.splitlines()
        for line in mlines:
            bug = find_bz_in_line(line, 'Bugzilla')
            if not bug:
                continue
            if bug in dependencies:
                dep_bzs.append(bug)
            elif mr_bugs and bug not in mr_bugs:
                LOGGER.debug("Bugzilla: %s not listed in MR description.", bug)
                non_mr_bzs.append(bug)
            else:
                bzs.append(bug)
    # We return empty arrays if no bugs are found
    return (bzs, non_mr_bzs, dep_bzs)


def extract_bzs(message):
    """Extract BZs from the message."""
    bzs, _x, _y = extract_all_bzs(message, [], [])
    return bzs


def extract_dependencies(description):
    """Extract Depends: bugs from MR description."""
    bzs = []

    if description:
        dlines = description.splitlines()
        for line in dlines:
            bug = find_bz_in_line(line, 'Depends')
            if not bug:
                continue
            bzs.append(bug)
    # We return an empty array if there are no bz deps
    return bzs
