"""Parser for the RHEL owners.yaml file."""

import fnmatch
import re

import yaml


class Entry:
    """Represents an entry in the owners.yaml file."""

    def __init__(self, entry):
        """Save an individual entry from the Parser class."""
        self._entry = entry

    def get_subsystem_name(self):
        """Getter for the subsystem name."""
        return self._entry.get('subsystem', None)

    def get_subsystem_label(self):
        """Getter for the subsystem label."""
        return self._entry.get('labels', {}).get('name', None)

    def get_ready_for_merge_label_deps(self):
        """Getter for the ready for merge deps."""
        return self._entry.get('labels', {}).get('readyForMergeDeps', [])

    def get_status(self):
        """Getter for the status."""
        return self._entry.get('status', None)

    def get_maintainers(self):
        """Getter for the maintainers."""
        return self._entry.get('maintainers', [])

    def get_reviewers(self):
        """Getter for the reviewers."""
        return self._entry.get('reviewers', [])

    def get_scm(self):
        """Getter for the SCM."""
        return self._entry.get('scm', None)

    def get_mailing_list(self):
        """Getter for the mailinglist."""
        return self._entry.get('mailingList', None)

    def get_path_include_regexes(self):
        """Getter for the include regexes."""
        return self._get_path('includeRegexes')

    def get_path_includes(self):
        """Getter for the include paths."""
        return self._get_path('includes')

    def get_path_excludes(self):
        """Getter for the include paths."""
        return self._get_path('excludes')

    def _get_path(self, subpart):
        paths = self._entry.get('paths', {})
        if not paths:
            paths = {}

        ret = paths.get(subpart, [])
        return ret if ret else []

    def _perform_match(self, filename):
        includes_match = _glob_matches(filename, self.get_path_includes()) or \
                         _regex_matches(filename, self.get_path_include_regexes())
        return includes_match and not _glob_matches(filename, self.get_path_excludes())

    def matches(self, filenames):
        """Determine if any of the filenames match the path includes and excludes."""
        return any(self._perform_match(x) for x in filenames)

    def __repr__(self):
        """Return the subsystem name as the string repesentation of the Entry class."""
        return self.get_subsystem_name()


class Parser:
    """YAML parser for the owners.yaml file."""

    def __init__(self, owner_yaml_contents):
        """Parse the string contents of the owners.yaml file."""
        if not owner_yaml_contents:
            self._entries = []
        else:
            owners = yaml.load(owner_yaml_contents, Loader=yaml.SafeLoader)
            self._entries = [Entry(entry) for entry in owners['subsystems']]

    def get_all_entries(self):
        """Return all of the entries in the owners.yaml file."""
        return self._entries

    def get_matching_entries(self, filenames):
        """Return all of the matching entries in the owners.yaml file."""
        return [x for x in self._entries if x.matches(filenames)]


def _regex_matches(filename, regexes):
    """Perform a regex match against multiple patterns."""
    return any(_regex_match(filename, x) for x in regexes)


def _regex_match(filename, regex):
    if not regex:
        return True

    return re.compile(regex).search(filename) is not None


def _glob_matches(filename, patterns):
    """Perform a glob match against multiple patterns."""
    return any(glob_match(filename, x) for x in patterns)


def glob_match(filename, pattern):
    """Check whether the filename matches the given shell-style pattern.

    Unlike fnmatch.fnmatchcase, the directories are matched correctly.
    """
    # Special case: a pattern ending with a slash matches everything under
    # the given directory.
    dir_match = False
    if pattern.endswith('/'):
        pattern = pattern[:-1]
        dir_match = True

    # Split to path components.
    fn_components = filename.split('/')
    pat_components = pattern.split('/')

    # If the pattern has more components, there is no way the filename can
    # match.
    if len(fn_components) < len(pat_components):
        return False

    # Check the path components one by one.
    for fn_one, pat_one in zip(fn_components, pat_components):
        if not fnmatch.fnmatchcase(fn_one, pat_one):
            return False

    # If we're matching everything under the pattern directory, it's okay if
    # there are extra components in the path. This is a match.
    if dir_match:
        return True

    # We may have more components in the filename than in the pattern. This
    # should generally not happen. Let's be conservative and return no
    # match in such case.
    return len(fn_components) == len(pat_components)
