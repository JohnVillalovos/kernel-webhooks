"""Public merge request handling."""
import sys

from cki_lib import logger

from . import common

CONFIG_LABEL_NAME = 'Configuration'
CONFIG_LABEL_COLOR = '#428BCA'
CONFIG_LABEL_DESC = 'Indicates an issue or merge request is related to kernel configurations.'

NO_CCS_LABEL_NAME = 'No CCs'
NO_CCS_LABEL_COLOR = '#428BCA'
NO_CCS_LABEL_DESC = 'Indicates a kernel configuration-related MR does not have any Ccs: defined.'

LOGGER = logger.get_logger('cki.webhook.public')


def _apply_config_label(gl_project, gl_mergerequest) -> None:
    """Add or remove the CONFIG_LABEL and NO_CCS_LABEL to merge requests."""
    config_dirs = [
        f"redhat/configs/{flavor}" for flavor in ("fedora", "common", "ark")]

    mr_has_ccs = False
    mr_affects_configuration = False
    labels_to_add = []
    if gl_mergerequest.description and 'Cc: ' in gl_mergerequest.description:
        mr_has_ccs = True
    for change in gl_mergerequest.changes()["changes"]:
        for config_dir in config_dirs:
            if change["old_path"].startswith(config_dir) or change[
                    "new_path"
            ].startswith(config_dir):
                mr_affects_configuration = True
                label = common.create_label_object(CONFIG_LABEL_NAME, CONFIG_LABEL_COLOR,
                                                   CONFIG_LABEL_DESC)
                labels_to_add.append(label)
                for commit in gl_mergerequest.commits():
                    if commit.message and 'Cc: ' in commit.message:
                        mr_has_ccs = True
                        break

    if mr_affects_configuration:
        if mr_has_ccs:
            common.remove_labels_from_merge_request(gl_project, gl_mergerequest.iid,
                                                    [NO_CCS_LABEL_NAME])
        else:
            label = common.create_label_object(NO_CCS_LABEL_NAME, NO_CCS_LABEL_COLOR,
                                               NO_CCS_LABEL_DESC)
            labels_to_add.append(label)
    else:
        common.remove_labels_from_merge_request(gl_project, gl_mergerequest.iid,
                                                [CONFIG_LABEL_NAME])

    if labels_to_add:
        common.add_label_to_merge_request(gl_project, gl_mergerequest.iid, labels_to_add)


def _do_process_mr(gl_instance, message, mr_id):
    gl_project = gl_instance.projects.get(message.payload["project"]["id"])
    gl_mergerequest = gl_project.mergerequests.get(mr_id)

    _apply_config_label(gl_project, gl_mergerequest)


def public_process_mr(gl_instance, message):
    """Process a merge request message."""
    if not common.mr_action_affects_commits(message):
        return

    _do_process_mr(gl_instance, message, message.payload["object_attributes"]["iid"])


def public_process_note(gl_instance, message):
    """Process a merge request only if request-public-evluation was specified."""
    if not common.force_webhook_evaluation(message.payload['object_attributes']['note'], 'public'):
        return

    _do_process_mr(gl_instance, message, message.payload["merge_request"]["iid"])


WEBHOOKS = {
    "merge_request": public_process_mr,
    "note": public_process_note,
}


def main(args):
    """Run main loop."""
    parser = common.get_arg_parser('PUBLIC')
    args = parser.parse_args(args)
    common.generic_loop(args, WEBHOOKS)


if __name__ == "__main__":
    main(sys.argv[1:])
