"""Add kernel subsystem topics as labels to an MR."""
import os
import re
import sys

from cki_lib import logger
from cki_lib import misc
from cki_lib import session
from yaml import safe_load

from . import cdlib
from . import common
from . import owners

LOGGER = logger.get_logger('cki.webhook.subsystems')
SESSION = session.get_session('cki.webhook.subsystems')

SUBSYS_LABEL_PREFIX = 'Subsystem'
SUBSYS_LABEL_COLOR = '#778899'
SUBSYS_LABEL_DESC = 'An MR that affects code related to %s.'
DRIVER_LABEL_PREFIX = 'Driver'
DRIVER_LABEL_COLOR = '#2f4f4f'
DRIVER_LABEL_DESC = 'An MR that affects code related to the %s driver.'
NEEDS_TESTING_LABEL_DESC = 'The subsystem-required %s testing has not yet been completed.'

NOTIFICATION_HEADER = "Notifying users:"
NOTIFICATION_TEMPLATE = ("{header} {users}  \nThis is the Subsystems hook's user notification"
                         " system for file changes. Please see the"
                         " [kernel-watch project]({project}) for details.")


def load_yaml_data(map_file):
    """Return the yaml data from the given file."""
    try:
        with open(map_file) as yaml_file:
            return safe_load(yaml_file)
    # pylint: disable=broad-except
    except Exception:
        LOGGER.exception('Problem loading yaml file.')
        return None


def path_matches(search_key, map_path_list, regex=False):
    # pylint: disable=too-many-return-statements
    """Return True if the key matches any of the path_list items."""
    if not map_path_list:
        return False
    # N: Files and directories *Regex* patterns.
    #  N:   [^a-z]tegra     all files whose path contains tegra
    #                       (not including files like integrator)
    if regex:
        for pattern in map_path_list:
            if re.search(pattern, search_key):
                return True
        return False

    # F: Files and directories with wildcard patterns.
    #  A trailing slash includes all files and subdirectory files.
    #  F:	drivers/net/	all files in and below drivers/net
    #  F:	drivers/net/*	all files in drivers/net, but not below
    #  F:	*/net/*		all files in "any top level directory"/net
    for map_path in map_path_list:
        if owners.glob_match(search_key, map_path):
            return True
    return False


def make_labels(topics):
    """Given a list of topics return a list of gitlab label objects."""
    label_list = []
    for topic in topics:
        if topic.startswith('driver '):
            driver_name = topic.split()[1]
            label_color = DRIVER_LABEL_COLOR
            label_description = DRIVER_LABEL_DESC % driver_name
            label = common.create_label_object(f'{DRIVER_LABEL_PREFIX}:{driver_name}',
                                               label_color, label_description)
        elif topic.endswith(common.NEEDS_TESTING_SUFFIX):
            label_color = common.NEEDS_TESTING_LABEL_COLOR
            label_description = NEEDS_TESTING_LABEL_DESC % topic.split(":")[0]
            label = common.create_label_object(f'{topic}', label_color, label_description)
        else:
            label_color = SUBSYS_LABEL_COLOR
            label_description = SUBSYS_LABEL_DESC % topic
            label = common.create_label_object(f'{SUBSYS_LABEL_PREFIX}:{topic}',
                                               label_color, label_description)
        label_list.append(label)
    return label_list


def user_wants_notification(user_data, path_list, target_branch):
    """Determine if the given user wants notification about any of the given files."""
    for path in path_list:
        if 'all' in user_data and path_matches(path, user_data['all']):
            return True
        if target_branch in user_data and path_matches(path, user_data[target_branch]):
            return True
    return False


def get_mr_pathlist(project, merge_request):
    """Get a file listing for the MR."""
    dep_label = cdlib.set_dependencies_label(project, merge_request)
    if dep_label == "Dependencies::OK":
        mr_changes = merge_request.changes().get('changes', [])
        old_paths = [path['old_path'] for path in mr_changes]
        new_paths = [path['new_path'] for path in mr_changes]
        return list(set(old_paths + new_paths))
    changed_files = cdlib.get_filtered_changed_files(project, merge_request)
    return changed_files


def get_current_subsystems_from_labels(gl_mergerequest):
    """Get the current subsystems from existing labels."""
    # One-liner equivalent of:
    # subsys_list = []
    # for label in gl_mergerequest.labels:
    #     if label.startswith(SUBSYS_LABEL_PREFIX):
    #         subsystem = label.split(":")[1]
    #         subsys_list.append(subsystem)
    # return subsys_list
    return [x.split(':')[1] for x in gl_mergerequest.labels if x.startswith(SUBSYS_LABEL_PREFIX)]


def get_stale_subsystems(old_subsystems, new_subsystems):
    """Get list of subsystems that should be removed from the ss labels."""
    return [x for x in old_subsystems if x not in new_subsystems]


def remove_stale_subsystem_labels(gl_project, mr_id, stale_subsystems):
    """Remove stale subsystem labels from the merge request."""
    remove = [f'{SUBSYS_LABEL_PREFIX}:{x}' for x in stale_subsystems]
    common.remove_labels_from_merge_request(gl_project, mr_id, remove)


def do_usermapping(target_branch, path_list, repo_path):
    """For the given branch and list of paths return a list of users to be notified."""
    # Every file in the repo 'users' directory should be the name of a GL user.
    users_path = os.path.join(repo_path, 'users')
    try:
        path_listing = os.listdir(users_path)
    except OSError:
        LOGGER.exception("Problem listing path: '%s'", users_path)
        return []

    user_list = []
    for username in path_listing:
        user_path = os.path.join(users_path, username)
        user_data = load_yaml_data(user_path)
        if not user_data:
            LOGGER.error("Error loading user data from path '%s'.", user_path)
            continue
        if user_wants_notification(user_data, path_list, target_branch):
            user_list.append(username)
    return user_list


def post_notifications(merge_request, user_list, kernel_watch_url):
    """Post a note to the MR notifying the users in user_list."""
    participants = []
    for participant in merge_request.participants():
        participants.append(participant['username'])

    new_users = ['@' + user for user in user_list if user not in participants]
    if not new_users:
        LOGGER.info('No one new to notify.')
        return

    note_text = NOTIFICATION_TEMPLATE.format(header=NOTIFICATION_HEADER, users=' '.join(new_users),
                                             project=kernel_watch_url)
    LOGGER.info('Posting notification on MR %d:\n%s', merge_request.iid, note_text)
    if misc.is_production():
        merge_request.notes.create({'body': note_text})


def _do_process_mr(gl_instance, message, mr_id, owners_parser, local_repo_path, kernel_watch_url):
    # pylint: disable=too-many-arguments,too-many-locals
    gl_project = gl_instance.projects.get(message.payload["project"]["id"])
    gl_mergerequest = gl_project.mergerequests.get(mr_id)

    # first process owners_parser and assign labels ...
    path_list = get_mr_pathlist(gl_project, gl_mergerequest)
    if not path_list:
        LOGGER.info("MR %s does not report any changed files, exiting.", mr_id)
        return

    entries = owners_parser.get_matching_entries(path_list)
    LOGGER.debug('Matching owners entries: %s', entries)

    old_subsystems = get_current_subsystems_from_labels(gl_mergerequest)
    new_subsystems = [x.get_subsystem_label() for x in entries if x.get_subsystem_label()]
    stale_subsystems = get_stale_subsystems(old_subsystems, new_subsystems)
    if stale_subsystems:
        LOGGER.info('Stale subsystems (to be removed): %s', stale_subsystems)
        remove_stale_subsystem_labels(gl_project, gl_mergerequest.iid, stale_subsystems)
    LOGGER.debug('Subsystem labels: %s', new_subsystems)
    if new_subsystems:
        common.add_label_to_merge_request(gl_project, gl_mergerequest.iid,
                                          make_labels(new_subsystems))
    else:
        LOGGER.info('No labels to add.')

    blocking_test_labels = [x.get_ready_for_merge_label_deps()
                            for x in entries if x.get_ready_for_merge_label_deps()]
    LOGGER.debug('Blocking test labels: %s', blocking_test_labels)
    if blocking_test_labels:
        for label in blocking_test_labels:
            label = f'{label[0]}::{common.NEEDS_TESTING_SUFFIX}'
            common.add_label_to_merge_request(gl_project, gl_mergerequest.iid,
                                              make_labels([f'{label}']))
    else:
        LOGGER.info('No blocking test labels to add.')

    # ... then do user notifications.
    target_branch = gl_mergerequest.target_branch \
        if gl_mergerequest.target_branch != 'main' else gl_project.namespace['name']

    user_list = do_usermapping(target_branch, path_list, local_repo_path)
    if user_list:
        post_notifications(gl_mergerequest, user_list, kernel_watch_url)
    else:
        LOGGER.info('No new users to notify @ for MR %d.', gl_mergerequest.iid)


def check_test_label_needs_update(payload):
    """Check for required blocking testing labels that need an update."""
    update = common.has_label_changed(payload, common.NEEDS_TESTING_SUFFIX,
                                      common.LabelPart.SUFFIX)
    update |= common.has_label_changed(payload, common.TESTING_FAILED_SUFFIX,
                                       common.LabelPart.SUFFIX)
    update |= common.has_label_changed(payload, common.READY_SUFFIX, common.LabelPart.SUFFIX)
    return update


def process_mr(gl_instance, msg, owners_parser, local_repo_path, kernel_watch_url):
    """Process a merge request message."""
    # If the MR file contents haven't changed and our labels haven't changed then don't run.
    commits_changed = common.mr_action_affects_commits(msg)
    label_changed = common.has_label_changed(msg.payload, f'{SUBSYS_LABEL_PREFIX}:',
                                             common.LabelPart.PREFIX)
    label_changed |= common.has_label_changed(msg.payload, f'{DRIVER_LABEL_PREFIX}:',
                                              common.LabelPart.PREFIX)
    test_label_update = check_test_label_needs_update(msg.payload)
    if not commits_changed and not label_changed and not test_label_update:
        return
    _do_process_mr(gl_instance, msg, msg.payload["object_attributes"]["iid"], owners_parser,
                   local_repo_path, kernel_watch_url)


WEBHOOKS = {
    "merge_request": process_mr,
}


def main(args):
    """Run main loop."""
    parser = common.get_arg_parser('SUBSYSTEMS')
    parser.add_argument('--owners-yaml', **common.get_argparse_environ_opts('OWNERS_YAML'),
                        help='Path to the owners.yaml file')
    parser.add_argument('--local-repo-path', **common.get_argparse_environ_opts('LOCAL_REPO_PATH'),
                        help='Local path where the kernel-watch repo is checked out')
    parser.add_argument('--kernel-watch-url',
                        **common.get_argparse_environ_opts('KERNEL_WATCH_URL'),
                        help='URL of the kernel-watch project')
    args = parser.parse_args(args)

    LOGGER.info('Using owners file: %s, local repo path: %s, kernel-watch repo: %s.',
                args.owners_yaml, args.local_repo_path, args.kernel_watch_url)
    owners_parser = common.get_owners_parser(args.owners_yaml)

    common.generic_loop(args, WEBHOOKS, owners_parser=owners_parser,
                        local_repo_path=args.local_repo_path,
                        kernel_watch_url=args.kernel_watch_url)


if __name__ == "__main__":
    main(sys.argv[1:])
